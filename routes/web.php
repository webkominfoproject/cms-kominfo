<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->get('/', 'AuthController@index')->name('index');

Auth::routes();

Route::middleware('guest')->get('/', function(){
    return view('auth.index');
})->name('index');

