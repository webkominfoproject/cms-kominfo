<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->middleware('auth')->group(function() {
    Route::get('/', 'UserController@index')->name('user.index');
    Route::get('data', 'UserController@data')->name('user.data');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
    Route::post('update/{id}', 'UserController@update')->name('user.update');
    Route::get('approve/{id}', 'UserController@approve')->name('user.approve');
    Route::get('decline/{id}', 'UserController@decline')->name('user.decline');
    Route::get('destroy/{id}', 'UserController@destroy')->name('user.destroy');
    Route::get('info', 'UserController@info')->name('user.info');
});

Route::prefix('role')->middleware('auth')->group(function() {
    Route::get('/', 'RoleController@index')->name('role.index');
    Route::get('data', 'RoleController@data')->name('role.data');
    Route::post('store', 'RoleController@store')->name('role.store');
    Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::post('update/{id}', 'RoleController@update')->name('role.update');
    Route::get('destroy/{id}', 'RoleController@destroy')->name('role.destroy');
    Route::get('info', 'RoleController@info')->name('role.info');
});

Route::prefix('profil')->middleware('auth', 'userdata')->group(function() {
    Route::get('/', 'UserController@profil')->name('profil.index');
    Route::post('updateprof', 'UserController@updateprofil')->name('profil.update');
    Route::post('updatepic', 'UserController@updateprofilpic')->name('profil.updatepic');
    Route::get('deletepic', 'UserController@deletepic')->name('profil.deletepic');
});
