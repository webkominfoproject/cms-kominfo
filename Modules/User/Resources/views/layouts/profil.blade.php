@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Website Management
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('pages.store') }}" method="POST">
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="home">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">PROFIL</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <a class="pull-right" href="#" id="add-data">
		                        <span class="btn btn-rounded btn-success">
		                            <i class="ti-reload"></i> UPDATE
		                        </span>
		                        </a>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
	                    <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Hashtag </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="" id="hashtag" name="hashtag">
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form>
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">ABOUT US</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <a class="pull-right" href="#" id="add-data">
		                        <span class="btn btn-rounded btn-success">
		                            <i class="ti-reload"></i> UPDATE
		                        </span>
		                        </a>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
	                    <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">About Us </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" type="text" id="description" name="description"></textarea>
                            </div>
                        </div>
                        
                	</div>
                </form>
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
@endsection
@section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                app.init();
                $('.select2').select2();
            })
            $(function () {
                $("#foto").change(function () {
                    var size = this.files[0].size;
                    if(size > 100000000){
                        $(this).addClass('is-invalid');
                    }else{
                        $(this).removeClass('is-invalid');
                    }
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            function imageIsLoaded(e) {
                $('#fotoimage').attr('src', e.target.result);
            };
        </script>
@endsection