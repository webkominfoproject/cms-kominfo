@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Website Management
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-4 col-sm-4">
            <div class="card m-b-20">
                <form id="form-profilpic" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h4 class="mt-10 m-b-10 header-title">PHOTO PROFILE</h4>
                            </div>
                            <div class="col-md-3 text-right">
                                <button type="submit" class="btn btn-rounded btn-success">
                                    <span>
                                        <i class="ti-reload"></i> UPDATE
                                    </span>
                                </button>
                            </div>
                            <div class="col-md-3 text-right">
                                <a href="{{ route('profil.deletepic') }}?id={{ $data->id }}">
                                    <button type="button" id="deletepic" class="btn btn-rounded btn-danger">
                                        <span>
                                            <i class="ti-close"></i> DELETE
                                        </span>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <img class="rounded" alt="200x200" id="fotoimage" width="100%" src="{{ $data->image == null ? asset('image/default-user.png') : env('MEDIA_URL').$data->image }}" data-holder-rendered="true">
                                </div>
                            </div>
                            <div class="col-md-12 text-left">
                                <div class="form-group">
                                    <input type="file" class="form-control" name="foto" id="foto" accept="image/*" required>
                                    <span class="help-block">
                                        <strong>Maximum file size : 100MB</strong>
                                    </span>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>File too large. Please change with smaller size.</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-xl-8 col-sm-8">
            <div class="card m-b-20">
            	<form id="form-profil" method="POST">
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="home">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">PROFILE</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">		                        
                                    <span>
		                              <i class="ti-reload"></i> UPDATE
		                            </span>
		                        </button>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
                        <input type="hidden" name="id" value="{{ $data->id }}">
	                    <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">First Name </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $data->first_name }}" id="first_name" name="first_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Last Name </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $data->last_name }}" id="last_name" name="last_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Email </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="email" value="{{ $data->email }}" id="email" name="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">NIK </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $data->nik }}" id="nik" name="nik">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Jabatan </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $data->jabatan }}" id="jabatan" name="jabatan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Instansi </label>
                            <div class="col-sm-10">
                                <select class="form-control select2" name="instansi_id" id="instansi_id" required>
                                    <option value="0" {{ 0 == $data->instansi_id ? 'selected' : '' }} >Tidak ada instansi</option>
                                    @foreach($instansi as $key)
                                        <option value="{{ $key->id }}" {{ $key->id == $data->instansi_id ? 'selected' : '' }}>{{ $key->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">New Password </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="password" value="" id="new_password" name="new_password">
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
@endsection
@section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                app.handleProfil();
                $('.select2').select2();
                
            })
            $(function () {
                $("#foto").change(function () {
                    var size = this.files[0].size;
                    if(size > 100000000){
                        $(this).addClass('is-invalid');
                    }else{
                        $(this).removeClass('is-invalid');
                    }
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            function imageIsLoaded(e) {
                $('#fotoimage').attr('src', e.target.result);
            };
        </script>
@endsection