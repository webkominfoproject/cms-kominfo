<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Guzzle;
use Yajra\Datatables\Datatables;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    //view page
    public function index()
    {
        $title = 'USER ROLE';
        return view('user::role')->withTitle($title);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = Role::get();

        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        $parameter = $request->only(['name']);

        $store = Role::create($parameter);
       
        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];
    
        return json_encode($data);
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $data = Role::where('id', $id)->first();
        
        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = Role::findOrFail($id);
        $model->name = $request->name;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = Role::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $data = Guzzle::get([], env('API_ADMIN_URL'), 'role', $request->session()->get('token_user'));

        $total = 0;
        foreach($data['data']->data as $key){
            ++$total;
        }

        $info = [
            'total' => $total
        ];
        return json_encode($info);
    }
}
