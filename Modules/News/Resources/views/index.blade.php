@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Website Management
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    <div class="state-graph">
                            <div class="text-danger" style="font-size: 20pt;margin-bottom: -5px;" id="totalunverified"></div>
                            <div class="info text-danger">UNVERIVIED {{ $title }}</div>
                    </div>
                    <div class="state-graph">
                        <div class="text-success" style="font-size: 20pt;margin-bottom: -5px;" id="totalverified"></div>
                        <div class="info text-success">VERIFIED {{ $title }}</div>
                    </div>
                    <div class="state-graph">
                        <div style="font-size: 20pt;margin-bottom: -5px;color:black" id="total"></div>
                        <div class="info">TOTAL {{ $title }}</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-6 text-left">
                        <h4 class="mt-0 m-b-30 header-title">{{ $title }} LIST</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="pull-right" href="#" id="add-data">
                        <span class="btn btn-rounded btn-success">
                            <i class="ti-plus"></i> CREATE {{ $title }}
                        </span>
                        </a>
                    </div>
                    </div>
                    <table id="dataTable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>News Title</th>
                            <th>Topics</th>
                            <th>Upload by</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
    @include('news::form.index')
@endsection
@section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                app.handleNewsPage();
                $('.select2').select2();
            })
            $(function () {
                $("#foto").change(function () {
                    var size = this.files[0].size;
                    if(size > 100000000){
                        $(this).addClass('is-invalid');
                    }else{
                        $(this).removeClass('is-invalid');
                    }
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            function imageIsLoaded(e) {
                $('#fotoimage').attr('src', e.target.result);
            };
        </script>
@endsection