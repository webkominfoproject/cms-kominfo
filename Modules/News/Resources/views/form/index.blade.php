<form method="POST" class="dataForm" id="dataForm" enctype='multipart/form-data'>
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="row" id="content-form">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Title :</label>
                    <input type="text" name="title" id="title" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nama_client" class="control-label">Description :</label>
                    <textarea type="text" name="desc" id="desc" class="form-control" required></textarea> 
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Slug :</label>
                    <input type="text" name="slug" id="slug" class="form-control" required readonly>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Topics :</label>
                    <select class="select2 form-control" name="topics_id" id="topics_id" data-placeholder="Choose Topics...">
                        @foreach ($topics as $key)
                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nama_client" class="control-label">Content :</label>
                    <textarea name="description" id="description" ></textarea>
                </div>
                
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Photo :</label>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="form-group">
                    <img class="rounded" alt="200x200" id="fotoimage" width="100%" src="{{ asset('image/placeholder-image.png') }}" data-holder-rendered="true">
                </div>
            </div>
            <div class="col-md-6 text-left">
                <div class="form-group">
                    <input type="file" class="form-control" name="foto" id="foto" accept="image/*" required>
                    <span class="help-block">
                        <strong>Maximum file size : 100MB</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>File too large. Please change with smaller size.</strong>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" id="content-file">
            
        </div>
        <input type="hidden" id="method" name="method">
        <input type="hidden" id="id" name="id">
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
        <button type="submit"  class="btn btn-success waves-effect waves-light">Save</button>
    </div>
</form>
    
