<?php

namespace Modules\News\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use App\Http\Model\News;
use App\Http\Model\Topics;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Validator;

class NewsController extends Controller
{
    //view page
    public function index()
    {
        $title = "NEWS";
        $topics = Topics::get();
        return view('news::index')->withTopics($topics)->withTitle($title);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = News::with(['topics','user'])->get();
        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        if($request->method == 'update'){
            $data = $this->update($request->id, $request);
            return $data;
        }else{
            $validator = Validator::make($request->all(), [
                'foto' => 'required|file|max:100000000',
            ]);

            if ($validator->fails()){
                $data = [
                    'status' => 2,
                    'message' => 'Fail Update Data'
                ];

                return json_encode($data);
            }else{
                $foto = Storage::disk('sftp')->put('news', $request->file('foto'));
                if ($foto) {
                    $model = new News();
                    $model->title = $request->title;
                    $model->description = $request->desc;
                    $model->content = $request->description;
                    $model->topics_id = $request->topics_id;
                    $model->slug = $request->slug;
                    $model->image = $foto;
                    $model->user_id = Auth::user()->id;
                    
                    if (!$model->save()){
                        $data = [
                            'status' => 2,
                            'message' => 'Fail Update Data'
                        ];
                        return json_encode($data);
                    }

                    $data = [
                        'status' => 1,
                        'message' => 'Success Update Data'
                    ];
                    return json_encode($data);
                }else{
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
                    return json_encode($data);
                }
            }
        }
        
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $data = News::where('id',$id)->first();

        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = News::findOrFail($id);
        $model->title = $request->title;
        $model->description = $request->desc;
        $model->content = $request->description;
        $model->slug = $request->slug;
        $model->topics_id = $request->topics_id;

        if(!empty($request->foto)){
            $delete = Storage::disk('sftp')->delete($model->image);
            $upload = Storage::disk('sftp')->put('news', $request->file('foto'));
            $model->image = $upload;
        }

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
            return json_encode($data);
        }else{
            
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
            return json_encode($data);
        }
    }

    //approve data
    public function approve($id, Request $request)
    {
        $model = News::findOrFail($id);
        $model->verified = 1;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //decline data
    public function decline($id, Request $request)
    {
        $model = News::findOrFail($id);
        $model->verified = 0;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = News::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = News::get();

        $verified = 0;
        $unverified = 0;
        $total = 0;
        foreach($model as $key){
            if($key->verified){
                ++$verified;
            }else{
                ++$unverified;
            }
        }

        $info = [
            'totalverified' => $verified,
            'totalunverified' => $unverified,
            'total' => $verified+$unverified
        ];
        return json_encode($info);
    }
}
