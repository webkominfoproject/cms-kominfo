<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('news')->middleware('auth')->group(function() {
    Route::get('/', 'NewsController@index')->name('news.index');
    Route::get('data', 'NewsController@data')->name('news.data');
    Route::post('store', 'NewsController@store')->name('news.store');
    Route::get('edit/{id}', 'NewsController@edit')->name('news.edit');
    Route::get('content/{id}', 'NewsController@content')->name('news.content');
    Route::get('approve/{id}', 'NewsController@approve')->name('news.approve');
    Route::get('decline/{id}', 'NewsController@decline')->name('news.decline');
    Route::post('update/{id}', 'NewsController@update')->name('news.update');
    Route::get('destroy/{id}', 'NewsController@destroy')->name('news.destroy');
    Route::get('info', 'NewsController@info')->name('news.info');
});