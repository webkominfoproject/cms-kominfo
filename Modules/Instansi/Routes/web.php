<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('instansi')->middleware('auth')->group(function() {
    Route::get('/', 'InstansiController@index')->name('instansi.index');
    Route::get('data', 'InstansiController@data')->name('instansi.data');
    Route::post('store', 'InstansiController@store')->name('instansi.store');
    Route::get('edit/{id}', 'InstansiController@edit')->name('instansi.edit');
    Route::post('update/{id}', 'InstansiController@update')->name('instansi.update');
    Route::get('destroy/{id}', 'InstansiController@destroy')->name('instansi.destroy');
    Route::get('info', 'InstansiController@info')->name('instansi.info');
});