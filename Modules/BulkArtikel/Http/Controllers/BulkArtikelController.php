<?php

namespace Modules\BulkArtikel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Model\Content;
use App\Http\Model\ContentDetail;
use App\Http\Model\Tags;
use App\Http\Model\Topics;
use App\Http\Model\ContentTags;
use App\Http\Model\Instansi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BulkArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $title = "ARTIKEL";
        $tags = Tags::get();
        $topics = Topics::get();
        $instansi = Instansi::get();
        return view('bulkartikel::index')->withTopics($topics)->withTags($tags)->withTitle($title)->withInstansi($instansi);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bulkartikel::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // return (json_encode($request->all()));

        for ($i=0 ; $i < count($request->namebulk); $i++ ){
            $model = new Content();
            $model->name = $request->namebulk[$i];
            $model->description = $request->descriptionbulk[$i];
            $model->category = $request->categorybulk;
            $model->topics_id = $request->topics_idbulk[$i];
            $model->instansi_id = $request->instansi_idbulk[$i];
            $model->slug = $request->slugbulk[$i];
            $model->user_id = Auth::user()->id;
            if(!$model->save()){
                $data = [
                    'status' => 2,
                    'message' => 'Fail Update Data'
                ];
                return json_encode($data);
            }else{
                $foto = Storage::disk('sftp')->put('artikel', $request->file('artikelbulk')[$i]);
                if ($foto) {
                    $detail = new ContentDetail();
                    $detail->content_id = $model->id;
                    $detail->category = 'artikel';
                    $detail->source = $foto;
                    $detail->save();
                    $index = $i+1;
                    if (isset($request->{'tagsbulk_'.(int)$index})){
                        foreach($request->{'tagsbulk_'.(int)$index} as $tags){
                            $storetags = new ContentTags;
                            $storetags->tags_id = $tags;
                            $storetags->content_id = $model->id;
                            $storetags->save(['timestamps' => false]);
                            }
                    }else{
                        $data = [
                            'status' => 2,
                            'message' => 'Fail Update Data'
                        ];
                        return json_encode($data);
                    }
                    $data = [
                        'status' => 1,
                        'message' => 'Success Update Data'
                    ];
                }else{
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
                    return json_encode($data);
                }
            }
        }
        return json_encode($data);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bulkartikel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bulkartikel::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addForm(Request $request)
    {
        $title = "Bulk Artikel";
        $tags = Tags::get();
        $topics = Topics::get();
        $instansi = Topics::get();
        return view('bulkartikel::form.index')->withTags($tags)->withTopics($topics)->withTitle($title)->withCounter($_GET['counterParam'])->withInstansi($instansi);
    }
}
