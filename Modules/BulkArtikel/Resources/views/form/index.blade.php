<div class="card m-b-20">
    <div class="card-body">
        <div class="modal-body">
            <h4 class="page-title">Artikel {{ $counter }}</h4>
                <div class="row" id="content-form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Nama Artikel :</label>
                            <input type="text" name="namebulk[]" id="namebulk_{{$counter}}" class="namebulk form-control" data-counter="{{$counter}}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Slug :</label>
                            <input type="text" name="slugbulk[]" id="slugbulk_{{$counter}}" class="slugbulk form-control" data-counter="{{$counter}}" required readonly>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Deskripsi Konten :</label>
                            <textarea name="descriptionbulk[]" id="descriptionbulk" ></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Instansi :</label>
                            <select class="select2 form-control" name="instansi_idbulk[]" id="instansi_idbulk" data-placeholder="Pilih Instansi..." required>
                                @foreach ($instansi as $key)
                                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Topik :</label>
                            <select class="select2 form-control" name="topics_idbulk[]" id="topics_idbulk" data-placeholder="Pilih Topik..." required>
                                @foreach ($topics as $key)
                                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tags :</label>
                            <select class="select2 form-control select2-multiple" name="tagsbulk_{{ $counter }}[]" id="tagsbulk" multiple="multiple" multiple data-placeholder="Pilih Tags..." required>
                                @foreach ($tags as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 text-left">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Artikel :</label>
                            <input type="file" class="form-control" name="artikelbulk[]" id="artikelbulk" accept=".pdf,.doc,.docx" required>
                            <span class="help-block">
                                <strong>Ukuran file maksimal : 100MB | Tipe file : pdf, doc, docx</strong>
                            </span>
                            <span class="invalid-feedback" role="alert">
                                <strong>Ukuran file terlalu besar !!</strong>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row" id="content-file">    
        </div>
        <!-- <input type="hidden" value="artikel" name="category">
        <input type="hidden" id="method" name="method">
        <input type="hidden" id="id" name="id"> -->
    </div>
    <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
        <button type="submit"  class="btn btn-success waves-effect waves-light">Save</button>
    </div> -->
</div>

    
