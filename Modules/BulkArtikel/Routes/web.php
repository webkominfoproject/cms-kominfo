<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('bulkartikel')->group(function() {
    Route::get('/', 'BulkArtikelController@index')->name('bulkartikel.index');
    Route::post('/upload', 'BulkArtikelController@store');
    Route::get('/add-form', 'BulkArtikelController@addForm');
});
