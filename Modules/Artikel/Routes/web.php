<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('artikel')->middleware('auth', 'userdata')->group(function() {
    Route::get('/', 'ArtikelController@index')->name('artikel.index');
    Route::get('data', 'ArtikelController@data')->name('artikel.data');
    Route::post('store', 'ArtikelController@store')->name('artikel.store');
    Route::get('edit/{id}', 'ArtikelController@edit')->name('artikel.edit');
    Route::get('content/{id}', 'ArtikelController@content')->name('artikel.content');
    Route::get('approve/{id}', 'ArtikelController@approve')->name('artikel.approve');
    Route::get('decline/{id}', 'ArtikelController@decline')->name('artikel.decline');
    Route::post('update/{id}', 'ArtikelController@update')->name('artikel.update');
    Route::get('destroy/{id}', 'ArtikelController@destroy')->name('artikel.destroy');
    Route::get('info', 'ArtikelController@info')->name('artikel.info');
});