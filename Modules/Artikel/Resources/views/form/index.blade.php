<form method="POST" class="dataForm" id="dataForm" enctype='multipart/form-data'>
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="row" id="content-form">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Nama Artikel :</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Slug :</label>
                    <input type="text" name="slug" id="slug" class="form-control" required readonly>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Deskripsi Artikel:</label>
                    <textarea name="description" id="description" required></textarea>
                </div>
                <div class="form-group">
                    <label class="control-label">Tanggal Produksi :</label>
                    <input name="production_date" id="production-date" class="form-control" required>
                </div>
                <div class="form-group">
                    <label class="control-label">Instansi :</label>
                    <select class="select2 form-control" name="instansi_id" id="instansi_id" data-placeholder="Pilih instansi..." required>
                        @foreach ($instansi as $key)
                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Kategori :</label>
                    <div class="alert alert-dark" role="alert">
                        @foreach ($category as $key)
                            {!! $key->description !!}
                            @break
                        @endforeach
                    </div>
                    <select class="select2 form-control" name="category_id" id="category_id" data-placeholder="Pilih Kategori..." required>
                        @foreach ($category as $key)
                            <option value="{{ $key->id }}" data-description="{{ $key->description }}" >{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Topik :</label>
                    <select class="select2 form-control" name="topics_id" id="topics_id" data-placeholder="Pilih topik..." required>
                        @foreach ($topics as $key)
                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Tags :</label>
                    <select class="select2 form-control select2-multiple" name="tags[]" id="tags" multiple="multiple" multiple data-placeholder="Choose Tags..." required>
                        @foreach ($tags as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12 text-left">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Artikel :</label>
                    <input type="file" class="form-control" name="artikel" id="artikel" accept=".pdf,.doc,.docx" required>
                    <span class="help-block">
                        <strong>Ukuran file maksimal : 100MB | Tipe file : pdf, doc, docx</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>Ukuran file terlalu besar !!</strong>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" id="content-file">
            
        </div>
        <input type="hidden" value="artikel" name="category">
        <input type="hidden" id="method" name="method">
        <input type="hidden" id="id" name="id">
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
        <button type="submit"  class="btn btn-success waves-effect waves-light">Simpan</button>
    </div>
</form>
    
