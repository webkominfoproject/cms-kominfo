@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Manajemen Konten
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    <div class="state-graph">
                            <div class="text-danger" style="font-size: 20pt;margin-bottom: -5px;" id="totalunverified"></div>
                            <div class="info text-danger">Artikel Belum Terferivikasi</div>
                    </div>
                    <div class="state-graph">
                        <div class="text-success" style="font-size: 20pt;margin-bottom: -5px;" id="totalverified"></div>
                        <div class="info text-success">Artikel Terferivikasi</div>
                    </div>
                    <div class="state-graph">
                        <div style="font-size: 20pt;margin-bottom: -5px;color:black" id="total"></div>
                        <div class="info">Total Artikel</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-4 text-left">
                        <h4 class="mt-0 m-b-30 header-title">Daftar Artikel</h4>
                    </div>
                    <div class="col-md-4 text-right">
                        <a class="pull-right" href="#" id="add-data">
                        <span class="btn btn-rounded btn-success">
                            <i class="ti-plus"></i> Tambah Artikel
                        </span>
                        </a>
                    </div>
                    <div class="col-md-4 text-right">
                        <a class="pull-right" href="{{ route('bulkartikel.index') }}" id="add-data">
                        <span class="btn btn-rounded btn-warning">
                            <i class="ti-plus"></i> Upload Banyak Artikel
                        </span>
                        </a>
                    </div>
                    </div>
                    <table id="dataTable" class="table table-responsive-sm table-bordered nowrap"  style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Artikel</th>
                            <th>Topik</th>
                            <th>Diupload oleh</th>
                            <th>Konten</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
    @include('artikel::form.index')
@endsection
@section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                app.handleArtikelPage();
                $('.select2').select2();
            })

            $("#category_id").change(function(){
                var desc = $(this).find(':selected').data('description');
                $( ".alert" ).empty();
                $( ".alert" ).append(desc);
            });
        </script>

        <script>
    $(window).bind("resize", function () {
        if ($(this).width() < 768) {
            $('table').removeClass('dt-responsive')
        }
    }).trigger('resize');
</script>
@endsection