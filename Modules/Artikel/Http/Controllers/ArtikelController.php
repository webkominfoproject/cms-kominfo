<?php

namespace Modules\Artikel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use App\Http\Model\Content;
use App\Http\Model\ContentDetail;
use App\Http\Model\Tags;
use App\Http\Model\Topics;
use App\Http\Model\ContentTags;
use App\Http\Model\Instansi;
use App\Http\Model\Category;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Validator;
use Carbon\Carbon;

class ArtikelController extends Controller
{
    //view page
    public function index(Request $request)
    {
        $title = "ARTIKEL";
        $tags = Tags::get();
        $topics = Topics::get();
        if ($request->get('role') != "superadmin"){
            $instansi = Instansi::where('id', $request->get('instansi'))->get();
        }else{
            $instansi = Instansi::get();
        }
        $category = Category::where('group', 'artikel')->get();
        return view('artikel::index')->withTopics($topics)->withTags($tags)->withTitle($title)->withInstansi($instansi)->withCategory($category);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = Content::where('category', '=', 'artikel');
        $data->with(['contentdetail','user','contenttags', 'topics', 'user.roles']);
        if ($request->get('role') != "superadmin"){
            $data->where('instansi_id', $request->get('instansi'))
                ->whereHas('user.roles', function($query){
                    $query->whereRaw('(roles.name = "admin")');
                });
        }
        $data->orderBy('content.created_at', 'DESC')->get();
        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        if($request->method == 'update'){
            $data = $this->update($request->id, $request);
            return $data;
        }else if($request->method == 'content'){
            $data = $this->updatecontent($request->id, $request);
            return $data;
        }else{
            $validator = Validator::make($request->all(), [
                'artikel' => 'required|file|max:100000000',
            ]);

            if ($validator->fails()){
                $data = [
                    'status' => 2,
                    'message' => 'Fail Update Data'
                ];

                return json_encode($data);
            }else{
                $model = new Content();
                $model->name = $request->name;
                $model->description = $request->description;
                $model->slug = $request->slug;
                $model->category = $request->category;
                $model->category_id = $request->category_id;
                $model->topics_id = $request->topics_id;
                $model->instansi_id = $request->instansi_id;
                $model->content_production = Carbon::createFromFormat('d-m-Y', $request->production_date ,'Asia/Jakarta')->format('Y/m/d');
                $model->user_id = Auth::user()->id;
                if(!$model->save()){
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
                    return json_encode($data);
                }else{
                    $foto = Storage::disk('sftp')->put('artikel', $request->file('artikel'));
                    if ($foto) {
                        $detail = new ContentDetail();
                        $detail->content_id = $model->id;
                        $detail->category = 'artikel';
                        $detail->source = $foto;
                        $detail->save();
                        if (isset($request->tags)){
                            foreach($request->tags as $tags){
                                $storetags = new ContentTags;
                                $storetags->tags_id = $tags;
                                $storetags->content_id = $model->id;
                                $storetags->save(['timestamps' => false]);
                            }
                        }
                        $data = [
                            'status' => 1,
                            'message' => 'Success Update Data'
                        ];
                        return json_encode($data);
                    }else{
                        $data = [
                            'status' => 2,
                            'message' => 'Fail Update Data'
                        ];
                        return json_encode($data);
                    }
                }
            }
        }
        
    }

    //get data for Content
    public function content($id, Request $request)
    {
        $artikel = ContentDetail::where('content_id',$id)->where('category','artikel')->first();

        $data = [
            'id' => $id,
            'artikel' => $artikel->source,
        ];

        return json_encode($data);
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $content = Content::with(['contentdetail','contenttags'])->where('id',$id)->first();
        foreach ($content->contenttags as $key) {
            $tags[] = $key->tags_id;
        }
        $data = [
            'data' => $content,
            'tags' => $tags
        ];
        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = Content::findOrFail($id);
        $model->name = $request->name;
        $model->description = $request->description;
        $model->topics_id = $request->topics_id;
        $model->instansi_id = $request->instansi_id;
        $model->category_id = $request->category_id;
        $model->content_production = Carbon::createFromFormat('d-m-Y', $request->production_date ,'Asia/Jakarta')->format('Y/m/d');
        $model->slug = $request->slug;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
            return json_encode($data);
        }else{
            $tags = ContentTags::where('content_id',$id)->delete();
            foreach($request->tags as $tags){
                $storetags = new ContentTags;
                $storetags->tags_id = $tags;
                $storetags->content_id = $id;
                $storetags->save(['timestamps' => false]);
            }

            if(!empty($request->artikel)){
                $artikel = ContentDetail::where('content_id',$id)->where('category','artikel')->first();
                $deletethumb = Storage::disk('sftp')->delete($artikel->source);
                $uploadthumb = Storage::disk('sftp')->put('artikel', $request->file('artikel'));
                $artikel->source = $uploadthumb;
                $artikel->update();
            }
            
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
            return json_encode($data);
        }
    }

    //update content
    public function updatecontent($id, Request $request)
    {
        if(!empty($request->artikel)){
            $artikel = ContentDetail::where('content_id',$id)->where('category','artikel')->first();
            $deletethumb = Storage::disk('sftp')->delete($artikel->source);
            $uploadthumb = Storage::disk('sftp')->put('artikel', $request->file('artikel'));
            $artikel->source = $uploadthumb;
            $artikel->update();
        }

        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];

        return json_encode($data);
    }

    //approve data
    public function approve($id, Request $request)
    {
        $model = Content::findOrFail($id);
        $model->verified = 1;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //decline data
    public function decline($id, Request $request)
    {
        $model = Content::findOrFail($id);
        $model->verified = 0;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = Content::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $detail = ContentDetail::where('content_id',$id);
            foreach ($detail->get() as $del){
                $deletedata = Storage::disk('sftp')->delete($del->source);
            }
            $detail->delete();
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = Content::where('category', 'artikel')->get();

        $verified = 0;
        $unverified = 0;
        $total = 0;
        foreach($model as $key){
            if($key->verified){
                ++$verified;
            }else{
                ++$unverified;
            }
        }

        $info = [
            'totalverified' => $verified,
            'totalunverified' => $unverified,
            'total' => $verified+$unverified
        ];
        return json_encode($info);
    }
}
