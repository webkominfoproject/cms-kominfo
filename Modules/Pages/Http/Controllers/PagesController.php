<?php

namespace Modules\Pages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use App\Http\Model\PagesContent;
use App\Http\Model\Topics;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Validator;

class PagesController extends Controller
{
    //view page
    public function index()
    {
        $title = "HALAMAN";
        $home = [ 
            'hashtag' => PagesContent::where('pages', 'home')->where('section','hashtag')->first(),
            'header_title' => PagesContent::where('pages', 'home')->where('section','header_title')->first(),
            'header_subtitle' => PagesContent::where('pages', 'home')->where('section','header_subtitle')->first(),
            'footer_text' => PagesContent::where('pages', 'home')->where('section','footer_text')->first(),
        ];
        $about = PagesContent::where('pages', 'about')->where('section', 'about')->first();
        $blog = PagesContent::where('pages', 'blog')->where('section', 'blog')->first();
        $headerhomepage = PagesContent::where('pages', 'homepage')->where('section', 'header')->get();
        $footerhomepage = PagesContent::where('pages', 'homepage')->where('section', 'footer')->first();

        return view('pages::index')->withHome($home)->withAbout($about)->withTitle($title)->withBlog($blog)->withHeaderhomepage($headerhomepage)->withFooterhomepage($footerhomepage);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = News::with(['topics','user'])->get();
        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        //home
        if ($request->pages == 'home') {
            $data = PagesContent::where('pages', 'home')->count();
            if ($data > 0) {
                $hashtag = PagesContent::where('pages','home')->where('section','hashtag')->first();
                $hashtag->pages = $request->pages;
                $hashtag->section = 'hashtag';
                $hashtag->content = $request->hashtag;
                $hashtag->update();

                $header_title = PagesContent::where('pages','home')->where('section','header_title')->first();
                $header_title->pages = $request->pages;
                $header_title->section = 'header_title';
                $header_title->content = $request->header_title;
                $header_title->update();

                $header_subtitle = PagesContent::where('pages','home')->where('section','header_subtitle')->first();
                $header_subtitle->pages = $request->pages;
                $header_subtitle->section = 'header_subtitle';
                $header_subtitle->content = $request->header_subtitle;
                $header_subtitle->update();

                $footer_text = PagesContent::where('pages','home')->where('section','footer_text')->first();
                $footer_text->pages = $request->pages;
                $footer_text->section = 'footer_text';
                $footer_text->content = $request->footer_text;
                $footer_text->update();

                return redirect()->route('pages.index');
            }else{
                $hashtag = new PagesContent();
                $hashtag->pages = $request->pages;
                $hashtag->section = 'hashtag';
                $hashtag->content = $request->hashtag;
                $hashtag->save();

                $header_title = new PagesContent();
                $header_title->pages = $request->pages;
                $header_title->section = 'header_title';
                $header_title->content = $request->header_title;
                $header_title->save();

                $header_subtitle = new PagesContent();
                $header_subtitle->pages = $request->pages;
                $header_subtitle->section = 'header_subtitle';
                $header_subtitle->content = $request->header_subtitle;
                $header_subtitle->save();

                $footer_text = new PagesContent();
                $footer_text->pages = $request->pages;
                $footer_text->section = 'footer_text';
                $footer_text->content = $request->footer_text;
                $footer_text->save();

                return redirect()->route('pages.index');
            }

        }

        if ($request->pages == 'about') {
            $data = PagesContent::where('pages', 'about')->count();
            if ($data > 0) {
                $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
                if ($foto){
                    $about = PagesContent::where('pages','about')->where('section','about')->first();
                    $about->pages = $request->pages;
                    $about->section = 'about';
                    $about->content = $request->description;
                    $about->image_source = $foto;
                    $about->update();
                }

                return redirect()->route('pages.index');
            }else{
                $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
                if ($foto){
                    $about = new PagesContent();
                    $about->pages = $request->pages;
                    $about->section = 'about';
                    $about->content = $request->description;
                    $about->image_source = $foto;
                    $about->save();
                }
                
                return redirect()->route('pages.index');
            }
        }

        if ($request->pages == 'blog') {
            $data = PagesContent::where('pages', 'blog')->count();
            if ($data > 0) {
                $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
                if ($foto){
                    $about = PagesContent::where('pages','blog')->where('section','blog')->first();
                    $about->pages = $request->pages;
                    $about->section = 'blog';
                    $about->content = '-';
                    $about->image_source = $foto;
                    $about->update();
                }

                return redirect()->route('pages.index');
            }else{
                $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
                if ($foto){
                    $about = new PagesContent();
                    $about->pages = $request->pages;
                    $about->section = 'blog';
                    $about->content = '-';
                    $about->image_source = $foto;
                    $about->save();
                }
                
                return redirect()->route('pages.index');
            }
        }

        if ($request->pages == 'homepage') {

            if (isset($request->foto)){
                /*
                $data = PagesContent::where('pages', 'homepage')->where('section','header')->count();
                if ($data > 0){
                    $datafoto = PagesContent::where('pages', 'homepage')->where('section','header')->get();
                    foreach ($datafoto as $list){
                        Storage::disk('sftp')->delete(env('MEDIA_URL').$list->image_source);
                    }
                    $datafoto = PagesContent::where('pages', 'homepage')->where('section','header')->delete();
                    for ($i = 0; $i < count($request->foto); $i++){
                        $foto = Storage::disk('sftp')->put('foto', $request->file('foto')[$i]);
                        if ($foto){
                            $about = new PagesContent();
                            $about->pages = $request->pages;
                            $about->section = 'header';
                            $about->content = '-';
                            $about->image_source = $foto;
                            $about->save();
                        }
                    }
                }else{
                */
                    for ($i = 0; $i < count($request->foto); $i++){
                        $foto = Storage::disk('sftp')->put('foto', $request->file('foto')[$i]);
                        if ($foto){
                            $about = new PagesContent();
                            $about->pages = $request->pages;
                            $about->section = 'header';
                            $about->content = '-';
                            $about->image_source = $foto;
                            $about->save();
                        }
                    }
                // }
            }
            return redirect()->route('pages.index');
        }

        if ($request->pages == 'footer') {
            $data = PagesContent::where('pages', 'homepage')->where('section', 'footer')->count();
            if ($data > 0) {
                $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
                if ($foto){
                    $about = PagesContent::where('pages','homepage')->where('section','footer')->first();
                    $about->pages = 'homepage';
                    $about->section = 'footer';
                    $about->content = '-';
                    $about->image_source = $foto;
                    $about->update();
                    
                    Storage::disk('sftp')->delete(env('MEDIA_URL').$about->image_source);
                }

                return redirect()->route('pages.index');
            }else{
                $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
                if ($foto){
                    $about = new PagesContent();
                    $about->pages = 'homepage';
                    $about->section = 'footer';
                    $about->content = '-';
                    $about->image_source = $foto;
                    $about->save();
                }
                
                return redirect()->route('pages.index');
            }
        }
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $data = News::where('id',$id)->first();

        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = News::findOrFail($id);
        $model->title = $request->title;
        $model->description = $request->desc;
        $model->content = $request->description;
        $model->topics_id = $request->topics_id;

        if(!empty($request->foto)){
            $delete = Storage::disk('sftp')->delete($model->image);
            $upload = Storage::disk('sftp')->put('news', $request->file('foto'));
            $model->image = $upload;
        }

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
            return json_encode($data);
        }else{
            
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
            return json_encode($data);
        }
    }

    //approve data
    public function approve($id, Request $request)
    {
        $model = News::findOrFail($id);
        $model->verified = 1;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //decline data
    public function decline($id, Request $request)
    {
        $model = News::findOrFail($id);
        $model->verified = 0;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        // $model = News::where('id', $id);
        $model = PagesContent::where('id', $id)->first();
        Storage::disk('sftp')->delete(env('MEDIA_URL').$model->image_source);

        $model = PagesContent::where('id', $id);
        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        // return json_encode($data);
        return redirect()->route('pages.index');
    }

    //get info data
    public function info(Request $request)
    {
        $model = News::get();

        $verified = 0;
        $unverified = 0;
        $total = 0;
        foreach($model as $key){
            if($key->verified){
                ++$verified;
            }else{
                ++$unverified;
            }
        }

        $info = [
            'totalverified' => $verified,
            'totalunverified' => $unverified,
            'total' => $verified+$unverified
        ];
        return json_encode($info);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addForm(Request $request)
    {
        return view('pages::layouts.form-image-dashboard')->withCounter($_GET['counterParam']);
    }
}
