<div class="col-md-6 text-center">
    <div class="form-group">
        <img class="rounded" alt="200x200" id="header-homepage-fotoimage-{{$counter}}" width="100%" src="{{ !empty($blog->image_source) ?  env('MEDIA_URL').$blog->image_source :  asset('image/placeholder-image.png')  }}" data-holder-rendered="true">
    </div>
</div>
<div class="col-md-6 text-left">
    <div class="form-group">
        <input type="file" class="form-control header-homepage-foto" name="foto[]" id="header-homepage-foto-{{$counter}}" accept="image/*" required data-counter={{$counter}}>
        <span class="help-block">
            <strong>Maximum file size : 100MB</strong>
        </span>
        <span class="invalid-feedback" role="alert">
            <strong>File too large. Please change with smaller size.</strong>
        </span>
    </div>
</div>