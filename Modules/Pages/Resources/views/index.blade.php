@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Manajemen Website
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('pages.store') }}" method="POST">
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="home">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">HALAMAN UTAMA</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">
			                        <span>
			                            <i class="ti-reload"></i> Terapkan
			                        </span>
		                        </button>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
	                    <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Hashtag </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $home['hashtag'] == null ? '-' : $home['hashtag']->content }}" id="hashtag" name="hashtag">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Judul Header </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $home['header_title'] == null ? '-' : $home['header_title']->content }}" id="header_title" name="header_title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Sub Judul Header </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $home['header_subtitle'] == null ? '-' : $home['header_subtitle']->content }}" id="header_subtitle" name="header_subtitle">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Tulisan Footer </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" value="{{ $home['footer_text'] == null ? '-' : $home['footer_text']->content }}" id="footer_text" name="footer_text">
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('pages.store') }}" method="POST" enctype='multipart/form-data'>
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="about">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">TENTANG KAMI</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">
			                        <span>
		                            <i class="ti-reload"></i> Terapkan
		                        </span>
		                        </button>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
	                    <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Tentang Kita </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" type="text" id="description" name="description">{{ $about == null ? '-' : $about->content }}</textarea>
                            </div>
                        </div>
                        <div class="row" id="content-file">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nama_client" class="control-label">Gambar</label>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="form-group">
                                    <img class="rounded" alt="200x200" id="fotoimage" width="100%" src="{{ !empty($about->image_source) ?  env('MEDIA_URL').$about->image_source :  asset('image/placeholder-image.png')  }}" data-holder-rendered="true">
                                </div>
                            </div>
                            <div class="col-md-6 text-left">
                                <div class="form-group">
                                    <input type="file" class="form-control" name="foto" id="foto" accept="image/*" required>
                                    <span class="help-block">
                                        <strong>Ukuran file maksimal : 100MB</strong>
                                    </span>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ukuran file terlalu besar !</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('pages.store') }}" method="POST" enctype='multipart/form-data'>
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="blog">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">BLOG</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">
			                        <span>
		                            <i class="ti-reload"></i> Terapkan
		                        </span>
		                        </button>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
                        <div class="row" id="content-file">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nama_client" class="control-label">Gambar</label>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="form-group">
                                    <img class="rounded" alt="200x200" id="fotoimage" width="100%" src="{{ !empty($blog->image_source) ?  env('MEDIA_URL').$blog->image_source :  asset('image/placeholder-image.png')  }}" data-holder-rendered="true">
                                </div>
                            </div>
                            <div class="col-md-6 text-left">
                                <div class="form-group">
                                    <input type="file" class="form-control" name="foto" id="foto" accept="image/*" required>
                                    <span class="help-block">
                                        <strong>Ukuran file  maksimal : 100MB</strong>
                                    </span>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ukuran file terlalu besar !!</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('pages.store') }}" method="POST" enctype='multipart/form-data'>
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="homepage">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-8 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">HALAMAN UTAMA HEADER</h4>
		                    </div>
		                    <div class="col-md-2 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">
			                        <span>
                                        <i class="ti-reload"></i> Terapkan
                                    </span>
		                        </button>
		                    </div>
                            <div class="col-md-2 text-right">
                                <button type="button" class="btn btn-rounded btn-warning" data-toggle="modal" data-target="#form-add-header-image">
			                        <span>
                                        <i class="ti-reload"></i> Tambah
                                    </span>
		                        </button>
                            </div>
		                </div>
	                </div>
                	<div class="card-body">
                        <div class="row" id="content-file-header-homepage">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nama_client" class="control-label">Gambar</label>
                                </div>
                            </div>
                            @if (!empty($headerhomepage))
                                @foreach ($headerhomepage as $list)
                                    <div class="col-md-5 text-center">
                                        <div class="form-group">
                                            <img class="rounded" alt="200x200" id="header-homepage-fotoimage" width="100%" src="{{ !empty($list->image_source) ?  env('MEDIA_URL').$list->image_source :  asset('image/placeholder-image.png')  }}" data-holder-rendered="true">
                                        </div>
                                    </div>
                                    <div class="col-md-5 text-left">
                                        <div class="form-group">
                                            <input type="file" class="form-control header-homepage-foto" name="foto[]" id="header-homepage-foto-" accept="image/*" required >
                                            <span class="help-block">
                                                <strong>Ukuran file maksimal : 100MB</strong>
                                            </span>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Ukuran file terlalu besar !!</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <a href="{{ url('pages/destroy/'.$list->id)}}" type="submit" class="btn btn-rounded btn-danger" data-id="">
                                            <span>
                                                <i class="ti-reload"></i> Hapus
                                            </span>
                                        </a>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('pages.store') }}" method="POST" enctype='multipart/form-data'>
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="footer">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title">Halaman Utama Footer</h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">
			                        <span>
		                            <i class="ti-reload"></i> Terapkan
		                        </span>
		                        </button>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
                        <div class="row" id="content-file">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nama_client" class="control-label">Gambar</label>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="form-group">
                                    <img class="rounded" alt="200x200" id="fotoimage-homepage-footer" width="100%" src="{{ !empty($footerhomepage->image_source) ?  env('MEDIA_URL').$footerhomepage->image_source :  asset('image/placeholder-image.png')  }}" data-holder-rendered="true">
                                </div>
                            </div>
                            <div class="col-md-6 text-left">
                                <div class="form-group">
                                    <input type="file" class="form-control" name="foto" id="foto-homepage-footer" accept="image/*" required>
                                    <span class="help-block">
                                        <strong>Ukuran file maksimal : 100MB</strong>
                                    </span>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ukuran file terlalu besar !!</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
<!-- Modal -->
<div class="modal fade" id="form-add-header-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Gambar Header Halaman Utama </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{ route('pages.store') }}" method="POST" enctype='multipart/form-data'>
        {{ csrf_field() }}
        <input type="hidden" name="pages" value="homepage">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <img class="rounded" alt="200x200" id="header-homepage-fotoimage-insert" width="100%" src="{{   asset('image/placeholder-image.png')  }}" data-holder-rendered="true">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-left">
                <div class="form-group">
                    <input type="file" class="form-control" name="foto[]" id="header-homepage-foto-insert" accept="image/*" required>
                    <span class="help-block">
                        <strong>Ukuran file maksimal : 100MB</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>Ukuran file terlalu besar !!</strong>
                    </span>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('data-content')
@endsection
@section('script')
<script type="text/javascript">
    var counter = 0;
    var baseURL = "{{url('')}}";
    var mediaURL = "{{ env('MEDIA_URL') }}";
    
    $(document).ready(function() {
        app.init();
        $('.select2').select2();
    })
    $(function () {
        $("#header-homepage-foto-insert").change(function () {
            var size = this.files[0].size;
            if(size > 100000000){
                $(this).addClass('is-invalid');
            }else{
                $(this).removeClass('is-invalid');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoadedHomepageHeader;
                reader.readAsDataURL(this.files[0]);
            }
        });

        $("#foto-homepage-footer").change(function () {
            var size = this.files[0].size;
            if(size > 100000000){
                $(this).addClass('is-invalid');
            }else{
                $(this).removeClass('is-invalid');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoadedHomepageFooter;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

    function imageIsLoadedHomepageHeader(e) {
        $('#header-homepage-fotoimage-insert').attr('src', e.target.result);
    };

    function imageIsLoadedHomepageFooter(e) {
        $('#fotoimage-homepage-footer').attr('src', e.target.result);
    };

    $( ".add-form-image" ).click(function() {
        counter++;

        $.ajax({
            type:'GET',
            url:'/pages/add-form',
            data:{counterParam: counter},
            success:function(r) {
                $( "#content-file-header-homepage" ).append( r );
            }
        });
    });
</script>
@endsection