<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('pages')->middleware('auth')->group(function() {
    Route::get('/', 'PagesController@index')->name('pages.index');
    Route::get('data', 'PagesController@data')->name('pages.data');
    Route::post('store', 'PagesController@store')->name('pages.store');
    Route::get('edit/{id}', 'PagesController@edit')->name('pages.edit');
    Route::get('content/{id}', 'PagesController@content')->name('pages.content');
    Route::get('approve/{id}', 'PagesController@approve')->name('pages.approve');
    Route::get('decline/{id}', 'PagesController@decline')->name('pages.decline');
    Route::post('update/{id}', 'PagesController@update')->name('pages.update');
    Route::get('destroy/{id}', 'PagesController@destroy')->name('pages.destroy');
    Route::get('info', 'PagesController@info')->name('pages.info');
    Route::get('/add-form', 'PagesController@addForm');
});
