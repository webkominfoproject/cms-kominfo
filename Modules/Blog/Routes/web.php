<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('fokus')->middleware('auth', 'userdata')->group(function() {
    Route::get('/', 'BlogController@index')->name('fokus.index');
    Route::get('data', 'BlogController@data')->name('fokus.data');
    Route::post('store', 'BlogController@store')->name('fokus.store');
    Route::get('edit/{id}', 'BlogController@edit')->name('fokus.edit');
    Route::get('content/{id}', 'BlogController@content')->name('fokus.content');
    Route::get('approve/{id}', 'BlogController@approve')->name('fokus.approve');
    Route::get('decline/{id}', 'BlogController@decline')->name('fokus.decline');
    Route::post('update/{id}', 'BlogController@update')->name('fokus.update');
    Route::get('destroy/{id}', 'BlogController@destroy')->name('fokus.destroy');
    Route::get('info', 'BlogController@info')->name('fokus.info');
});