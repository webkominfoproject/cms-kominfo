@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Dashboard
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- <div class="col-xl-3 col-md-6"> -->
        <!-- <div class="col-md-12"> -->
            <div class="col-md-4">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-comment-text-outline float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Total User Terverifikasi</h6>
                            <h4 class="mb-4">{{ $total['userverified'] }}</h4>
                            <!-- <a href="javascript:void(0)" class="btn btn-info"> Lihat lebih </a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-comment-text-outline float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Total User Belum Terverifikasi</h6>
                            <h4 class="mb-4">{{ $total['userunverified'] }}</h4>
                            <!-- <a href="javascript:void(0)" class="btn btn-info"> Lihat lebih </a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-comment-text-outline float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Total Pengunjung</h6>
                            <h6 class="mb-4">{{ $total['visitortoday'] }} Pengunjung Hari Ini</h6>
                            <h6 class="mb-4">{{ $total['visitorweek'] }} Pengunjung 7 hari terakhir</h6>
                            <h6 class="mb-4">{{ $total['visitormonth'] }} Pengunjung 30 hari terakhir</h6>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </div>
    <div class="row">
        <div class="col-xl-3 col-md-6 col-sm-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Status Video Konten</h4>

                    <div class="row text-center m-t-20">
                        <div class="col-4">
                            <h5 class="text-info">{{ $total['video'] }}</h5>
                            <p class="text-info">Total</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-success">{{ $total['videoverified'] }}</h5>
                            <p class="text-success">Terverifikasi</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-danger">{{ $total['videounverified'] }}</h5>
                            <p class="text-danger">Tidak Terverifikasi</p>
                        </div>
                    </div>

                    <div id="video-donut" class="dashboard-charts morris-charts"></div>

                    <div class="row m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak diunduh :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="video" data-id="{{ $datavideo['mostdownload'] == null ? '-' : $datavideo['mostdownload']->content_id }}">
                                <b>{{ $datavideo['mostdownload'] == null ? '-' : $datavideo['mostdownload']['name'].'. Diunggah oleh '.$datavideo['mostdownload']['first_name'].' '.$datavideo['mostdownload']['last_name'] }} </b>
                            </a>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak dibagikan :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="video" data-id="{{ $datavideo['mostdownload'] == null ? '-' : $datavideo['mostdownload']->content_id }}">
                                <b>{{ $datavideo['mostshare'] == null ? '-' : $datavideo['mostshare']['name'].'. Diunggah oleh '.$datavideo['mostshare']['first_name'].' '.$datavideo['mostshare']['last_name'] }}  </b>
                            </a>
                        </div>
                    </div>
                    <div class="row  m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak dilihat :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="video" data-id="{{ $datavideo['mostdownload'] == null ? '-' : $datavideo['mostdownload']->content_id }}">
                                <b>{{ $datavideo['mostview'] == null ? '-' : $datavideo['mostview']['name'].'. Diunggah oleh '.$datavideo['mostview']['first_name'].' '.$datavideo['mostview']['last_name'] }}  </b>
                            </a>
                        </div>
                        <!-- <div class="col-12 d-flex justify-content-center">
                            <a href="{{ route('report.index', ['type' => 'user']) }}" type="button" class="btn btn-primary">Download</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 col-sm-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Status konten foto</h4>

                    <div class="row text-center m-t-20">
                        <div class="col-4">
                            <h5 class="text-info">{{ $total['photo'] }}</h5>
                            <p class="text-info">Total</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-success">{{ $total['photoverified'] }}</h5>
                            <p class="text-success">Terferifikasi</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-danger">{{ $total['photounverified'] }}</h5>
                            <p class="text-danger">Tidak terverifikasi</p>
                        </div>
                    </div>

                    <div id="photo-donut" class="dashboard-charts morris-charts"></div>

                    <div class="row m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak diunduh :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="foto" data-id="{{ $datafoto['mostdownload'] == null ? '-' : $datafoto['mostdownload']->content_id }}">
                                <b>{{ $datafoto['mostdownload'] == null  ? '-' : $datafoto['mostdownload']['name'].'. Diunggah oleh '.$datafoto['mostdownload']['first_name'].' '.$datafoto['mostdownload']['last_name'] }} </b>
                            </a>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak dibagikan :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="foto" data-id="{{ $datafoto['mostdownload'] == null ? '-' : $datafoto['mostdownload']->content_id }}">
                                <b>{{ $datafoto['mostshare'] == null  ? '-' : $datafoto['mostshare']['name'].'. Diunggah oleh '.$datafoto['mostshare']['first_name'].' '.$datafoto['mostshare']['last_name'] }}  </b>
                            </a>
                        </div>
                    </div>
                    <div class="row  m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak dilihat :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="foto" data-id="{{ $datafoto['mostdownload'] == null ? '-' : $datafoto['mostdownload']->content_id }}">
                                <b>{{ $datafoto['mostview'] == null ? '-' : $datafoto['mostview']['name'].'. Diunggah oleh '.$datafoto['mostview']['first_name'].' '.$datafoto['mostview']['last_name'] }}  </b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 col-sm-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Status konten artikel</h4>

                    <div class="row text-center m-t-20">
                        <div class="col-4">
                            <h5 class="text-info">{{ $total['artikel'] }}</h5>
                            <p class="text-info">Total</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-success">{{ $total['artikelverified'] }}</h5>
                            <p class="text-success">Terfirivikasi</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-danger">{{ $total['artikelunverified'] }}</h5>
                            <p class="text-danger">Tidak terverifikasi</p>
                        </div>
                    </div>

                    <div id="article-donut" class="dashboard-charts morris-charts"></div>

                    <div class="row m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak diunduh :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="artikel" data-id="{{ $dataartikel['mostdownload'] == null ? '-' : $dataartikel['mostdownload']->content_id }}">
                                <b>{{ $dataartikel['mostdownload'] == null ? '-' : $dataartikel['mostdownload']['name'].' Diunggah oleh '.$dataartikel['mostdownload']['first_name'].' '.$dataartikel['mostdownload']['last_name'] }} </b>
                            </a>
                        </div>
                    </div>
                    <div class="row m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak dibagikan :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="artikel" data-id="{{ $dataartikel['mostdownload'] == null ? '-' : $dataartikel['mostdownload']->content_id }}">
                                <b>{{ $dataartikel['mostshare'] == null ? '-' : $dataartikel['mostshare']['name'].'. Diunggah oleh '.$dataartikel['mostshare']['first_name'].' '.$dataartikel['mostshare']['last_name'] }}  </b>
                            </a>
                        </div>
                    </div>
                    <div class="row  m-t-15">
                        <div class="col-12" style="margin-bottom: -15px">
                            <p>Konten paling banyak dilihat :</p>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0)" class="content_detail" data-category="artikel" data-id="{{ $dataartikel['mostdownload'] == null ? '-' : $dataartikel['mostdownload']->content_id }}">
                                <b>{{ $dataartikel['mostview'] == null ? '-' : $dataartikel['mostview']['name'].'. Diunggah oleh '.$dataartikel['mostview']['first_name'].' '.$dataartikel['mostview']['last_name'] }}  </b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 col-sm-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Status Pengunjung</h4>

                    <div class="row text-center m-t-20">
                        <div class="col-4">
                            <h5 class="text-info">{{ $total['visitortoday'] }}</h5>
                            <p class="text-info">Total Pengunjung Hari Ini</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-success">{{ $total['visitorweek'] }}</h5>
                            <p class="text-success">Total Pengunjung 7 Hari Terakhir</p>
                        </div>
                        <div class="col-4">
                            <h5 class="text-danger">{{ $total['visitormonth'] }}</h5>
                            <p class="text-danger">Total Pengunjung 30 Hari Terakhir</p>
                        </div>
                    </div>

                    <div id="visitor-donut" class="dashboard-charts morris-charts"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
   
@endsection

@section('script')
        <script type="text/javascript">
            function createDonutChart(element, data, colors){
                 Morris.Donut({
                    element: element,
                    data: data,
                    resize: true,
                    colors: colors
                });
            };
            $(document).ready(function() {
                app.init();
                var $videoData = [
                    {label: "Konten diunduh", value: {{ $datavideo['downloaded'] }} },
                    {label: "Konten dibagikan", value: {{ $datavideo['shared'] }} },
                    {label: "Konten dilihat", value: {{ $datavideo['viewed'] }} }
                ];
                var $photoData = [
                    {label: "Konten diunduh", value: {{ $datafoto['downloaded'] }} },
                    {label: "Konten dibagikan", value: {{ $datafoto['shared'] }} },
                    {label: "Konten dilihat", value: {{ $datafoto['viewed'] }} }
                ];
                var $articleData = [
                    {label: "Konten diunduh", value: {{ $dataartikel['downloaded'] }} },
                    {label: "Konten dibagikan", value: {{ $dataartikel['shared'] }} },
                    {label: "Konten dilihat", value: {{ $dataartikel['viewed'] }} }
                ];
                var $pengunjungData = [
                    {label: "Hari ini", value: {{ $total['visitortoday'] }} },
                    {label: "7 Hari terakhir", value: {{ $total['visitorweek'] }} },
                    {label: "30 Hari terakhir", value: {{ $total['visitormonth'] }} }
                ];

                createDonutChart('video-donut', $videoData, ['#f0f1f4', '#10a9d7', '#fab249']);  
                createDonutChart('photo-donut', $photoData, ['#f0f1f4', '#10a9d7', '#fab249']);  
                createDonutChart('article-donut', $articleData, ['#f0f1f4', '#10a9d7', '#fab249']); 
                createDonutChart('visitor-donut', $pengunjungData, ['#f0f1f4', '#10a9d7', '#fab249']); 

                // $('.content_detail').on('click',function(){
                //     id = $(this).data('id');
                //     category = $(this).data('category');


                // }); 
            });
        </script>
@endsection