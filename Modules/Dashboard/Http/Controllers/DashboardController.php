<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Model\Content;
use App\Http\Model\Blog;
use App\Http\Model\News;
use App\Http\Model\MemberActivity;
use App\User;
use DB;
use Analytics;
use Spatie\Analytics\Period;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        $role = Auth::user()->roles->first()->name;
        $id = Auth::user()->id;
        $title = 'DASHBOARD';

        $totalVisitorToday = $this::analytics(0);
        $totalVisitorWeek = $this::analytics(7);
        $totalVisitorMonth = $this::analytics(30);

        if ($role == 'admin') {
            $total = [
                'blog' => Blog::where('verified', 1)->where('user_id', $id)->count(),
                'news' => News::where('verified', 1)->where('user_id', $id)->count(),
                'video' => Content::where('category', 'video')->where('user_id', $id)->count(),
                'videoverified' => Content::where('category', 'video')->where('user_id', $id)->where('verified', 1)->count(),
                'videounverified' => Content::where('category', 'video')->where('user_id', $id)->where('verified', 0)->count(),
                'photo' => Content::where('category', 'foto')->where('user_id', $id)->count(),
                'photoverified' => Content::where('category', 'foto')->where('user_id', $id)->where('verified', 1)->count(),
                'photounverified' => Content::where('category', 'foto')->where('user_id', $id)->where('verified', 0)->count(),
                'artikel' => Content::where('category', 'artikel')->where('user_id', $id)->count(),
                'artikelverified' => Content::where('category', 'artikel')->where('user_id', $id)->where('verified', 1)->count(),
                'artikelunverified' => Content::where('category', 'artikel')->where('user_id', $id)->where('verified', 0)->count(),
                'userverified' => User::where('status', 1)->count(),
                'userunverified' => User::where('status', 0)->count(),
                'visitortoday' => $totalVisitorToday,
                'visitorweek' => $totalVisitorWeek,
                'visitormonth' => $totalVisitorMonth,
            ];

            $datavideo = [
                'downloaded' => MemberActivity::with('content')->where('activity', 'download')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'video');
                                })->count(),
                'viewed' => MemberActivity::with('content')->where('activity', 'view')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'video');
                                })->count(),
                'shared' => MemberActivity::with('content')->where('activity', 'share')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'video');
                                })->count(),
                'mostdownload' => $this->getMaxUser($id, 'video', 'download'),
                'mostview' => $this->getMaxUser($id, 'video', 'view'),
                'mostshare' => $this->getMaxUser($id, 'video', 'share'),
            ];

            $datafoto = [
                'downloaded' => MemberActivity::with('content')->where('activity', 'download')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'foto');
                                })->count(),
                'viewed' => MemberActivity::with('content')->where('activity', 'view')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'foto');
                                })->count(),
                'shared' => MemberActivity::with('content')->where('activity', 'share')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'foto');
                                })->count(),
                'mostdownload' => $this->getMaxUser($id,'foto', 'download'),
                'mostview' => $this->getMaxUser($id,'foto', 'view'),
                'mostshare' => $this->getMaxUser($id,'foto', 'share'),
            ];

            $dataartikel = [
                'downloaded' => MemberActivity::with('content')->where('activity', 'download')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'artikel');
                                })->count(),
                'viewed' => MemberActivity::with('content')->where('activity', 'view')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'artikel');
                                })->count(),
                'shared' => MemberActivity::with('content')->where('activity', 'share')->whereHas('content', function ($query) {
                                    $query->where('user_id', Auth::user()->id)->where('category', '=', 'artikel');
                                })->count(),
                'mostdownload' => $this->getMaxUser($id,'artikel', 'download'),
                'mostview' => $this->getMaxUser($id,'artikel', 'view'),
                'mostshare' => $this->getMaxUser($id,'artikel', 'share'),
            ];

            return view('dashboard::index')
                    ->withDatavideo($datavideo)->withDatafoto($datafoto)->withDataartikel($dataartikel)
                    ->withTotal($total)->withTitle($title);
        }
        $total = [
            'blog' => Blog::where('verified', 1)->count(),
            'news' => News::where('verified', 1)->count(),
            'video' => Content::where('category', 'video')->count(),
            'videoverified' => Content::where('category', 'video')->where('verified', 1)->count(),
            'videounverified' => Content::where('category', 'video')->where('verified', 0)->count(),
            'photo' => Content::where('category', 'foto')->count(),
            'photoverified' => Content::where('category', 'foto')->where('verified', 1)->count(),
            'photounverified' => Content::where('category', 'foto')->where('verified', 0)->count(),
            'artikel' => Content::where('category', 'artikel')->count(),
            'artikelverified' => Content::where('category', 'artikel')->where('verified', 1)->count(),
            'artikelunverified' => Content::where('category', 'artikel')->where('verified', 0)->count(),
            'userverified' => User::where('status', 1)->count(),
            'userunverified' => User::where('status', 0)->count(),
            'visitortoday' => $totalVisitorToday,
            'visitorweek' => $totalVisitorWeek,
            'visitormonth' => $totalVisitorMonth,
        ];

        $datavideo = [
            'downloaded' => MemberActivity::with('content')->where('activity', 'download')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'video');
                            })->count(),
            'viewed' => MemberActivity::with('content')->where('activity', 'view')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'video');
                            })->count(),
            'shared' => MemberActivity::with('content')->where('activity', 'share')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'video');
                            })->count(),
            'mostdownload' => $this->getMax('video', 'download'),
            'mostview' => $this->getMax('video', 'view'),
            'mostshare' => $this->getMax('video', 'share'),
        ];

        $datafoto = [
            'downloaded' => MemberActivity::with('content')->where('activity', 'download')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'foto');
                            })->count(),
            'viewed' => MemberActivity::with('content')->where('activity', 'view')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'foto');
                            })->count(),
            'shared' => MemberActivity::with('content')->where('activity', 'share')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'foto');
                            })->count(),
            'mostdownload' => $this->getMax('foto', 'download'),
            'mostview' => $this->getMax('foto', 'view'),
            'mostshare' => $this->getMax('foto', 'share'),
        ];

        $dataartikel = [
            'downloaded' => MemberActivity::with('content')->where('activity', 'download')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'artikel');
                            })->count(),
            'viewed' => MemberActivity::with('content')->where('activity', 'view')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'artikel');
                            })->count(),
            'shared' => MemberActivity::with('content')->where('activity', 'share')->whereHas('content', function ($query) {
                                $query->where('category', '=', 'artikel');
                            })->count(),
            'mostdownload' => $this->getMax('artikel', 'download'),
            'mostview' => $this->getMax('artikel', 'view'),
            'mostshare' => $this->getMax('artikel', 'share'),
        ];
        $returndata = [
            'total' => $total,
            'datavideo' => $datavideo,
            'datafoto' => $datafoto,
            'dataartikel' => $dataartikel,
        ];

        return view('dashboard::index')
                ->withDatavideo($datavideo)->withDatafoto($datafoto)->withDataartikel($dataartikel)
                ->withTotal($total)->withTitle($title);
    }

    public function getMax($category, $activity)
    {
        $coba = new MemberActivity();
        $data = $coba->select('*',DB::raw('count(*) as total'))
                    ->join('content','content.id','member_activity.content_id')
                    ->join('users','users.id','content.user_id')
                    ->where('member_activity.activity', $activity)
                    ->where('content.category', $category)
                    ->groupBy('content_id')
                    ->get();

        $return = $data->where('total',$data->max('total'))->first();

        return $return;
    }

    public function getMaxUser($id,$category, $activity)
    {
        $coba = new MemberActivity();
        $data = $coba->select('*',DB::raw('count(*) as total'))
                    ->join('content','content.id','member_activity.content_id')
                    ->join('users','users.id','content.user_id')
                    ->where('member_activity.activity', $activity)
                    ->where('content.category', $category)
                    ->where('content.user_id', $id)
                    ->groupBy('content_id')
                    ->get();

        $return = $data->where('total',$data->max('total'))->first();

        return $return;
    }

    public function data()
    {
        if ($request->id != null) {
            
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dashboard::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('dashboard::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('dashboard::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function analytics($days){
        $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::days($days));
        $total = 0;
        if (!empty($analyticsData)){
            foreach ($analyticsData as $data){
                $total = $total + $data['visitors'];
            }
        }
        
        return $total;
    }
}
