@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Manajemen Konten
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    <div class="state-graph">
                            <div class="text-danger" style="font-size: 20pt;margin-bottom: -5px;" id="totalunverified"></div>
                            <div class="info text-danger">Video Belum Terverifikasi</div>
                    </div>
                    <div class="state-graph">
                        <div class="text-success" style="font-size: 20pt;margin-bottom: -5px;" id="totalverified"></div>
                        <div class="info text-success">Vide Terverifikasi</div>
                    </div>
                    <div class="state-graph">
                        <div style="font-size: 20pt;margin-bottom: -5px;color:black" id="total"></div>
                        <div class="info">Total Video</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-4 text-left">
                        <h4 class="mt-0 m-b-30 header-title">Daftar Video</h4>
                    </div>
                    <div class="col-md-3 text-right l-text-m">
                        <a class="pull-right" href="#" id="add-data">
                        <span class="btn btn-rounded btn-success">
                            <i class="ti-plus"></i> Tambah Video
                        </span>
                        </a>
                    </div>

                    <div class="col-md-2 text-center l-text-m">
                        <a class="pull-right" href="#" id="add-youtube">
                        <span class="btn btn-rounded btn-success">
                            <i class="ti-plus"></i> Embed Video Youtube
                        </span>
                        </a>
                    </div>

                    <div class="col-md-3 text-right l-text-m mb-m-10">
                        <a class="pull-right" href="{{ route('bulkvideo.index') }}" id="add-data">
                        <span class="btn btn-rounded btn-warning">
                            <i class="ti-plus"></i> Tambah Banyak Video
                        </span>
                        </a>
                    </div>
                    </div>
                    <table id="dataTable" class="table table-responsive-sm table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Video</th>
                            <th>Deskripsi Video</th>
                            <th>Diupload Oleh</th>
                            <th>Konten</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
    @include('video::form.index')
@endsection
@section('script')

        <script type="text/javascript">
            $(document).ready(function() {
                app.handleVideoPage();
                $('.select2').select2();
                
            })
            $(function () {
                $("#thumbnail").change(function () {
                    var size = this.files[0].size;
                    if(size > 100000000){
                        $(this).addClass('is-invalid');
                    }else{
                        $(this).removeClass('is-invalid');
                    }
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });

                $("#category_id").change(function(){
                    var desc = $(this).find(':selected').data('description');
                    $( ".alert" ).empty();
                    $( ".alert" ).append(desc);
                });
            });

            function imageIsLoaded(e) {
                $('#thumbnailimage').attr('src', e.target.result);
            };
            $('#shortvideo').on("change", function(evt) {
                var $source = $('#prevshortvideo');
                var size = this.files[0].size;
                if(size > 200000000){
                    $(this).addClass('is-invalid');
                }else{
                    $(this).removeClass('is-invalid');
                }
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
            });
            $('#fullvideo').on("change", function(evt) {
                var $source = $('#prevfullvideo');
                var size = this.files[0].size;
                if(size > 200000000){
                    $(this).addClass('is-invalid');
                }else{
                    $(this).removeClass('is-invalid');
                }
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
            });
            $('#fullvideosimple').on("change", function(evt) {
                var $source = $('#prevfullvideosimple');
                var size = this.files[0].size;
                if(size > 200000000){
                    $(this).addClass('is-invalid');
                }else{
                    $(this).removeClass('is-invalid');
                }
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
            });
        </script>
        <script>
            $(window).bind("resize", function () {
                if ($(this).width() < 700) {
                    $('table').removeClass('dt-responsive')
                } 
            }).trigger('resize');
        </script>
        
@endsection