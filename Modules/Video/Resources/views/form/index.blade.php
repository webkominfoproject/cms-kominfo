<form method="POST" class="dataForm" id="dataForm" enctype='multipart/form-data'>
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="row" id="content-form">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Nama Video :</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Slug :</label>
                    <input type="text" name="slug" id="slug" class="form-control" required readonly>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Deskripsi Video :</label>
                    <textarea name="description" id="description" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label class="control-label">Tanggal Produksi :</label>
                    <input name="production_date" id="production-date" class="form-control" required>
                </div>
                <div class="form-group">
                    <label class="control-label">Instansi :</label>
                    <select class="select2 form-control" name="instansi_id" id="instansi_id" data-placeholder="Pilih Instansi..." required>
                        @foreach ($instansi as $key)
                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Topik :</label>
                    <select class="select2 form-control" name="topics_id" id="topics_id" data-placeholder="Pilih Topik..." required>
                        @foreach ($topics as $key)
                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Kategori :</label>
                    <div class="alert alert-dark" role="alert">
                        @foreach ($category as $key)
                            {!! $key->description !!}
                            @break
                        @endforeach
                    </div>
                    <select class="select2 form-control" name="category_id" id="category_id" data-placeholder="Pilih Kategori..." required>
                        @foreach ($category as $key)
                        <option value="{{ $key->id }}" data-description="{{ $key->description }}">{{ $key->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Tags :</label>
                    <select class="select2 form-control select2-multiple" name="tags[]" id="tags" multiple="multiple" multiple data-placeholder="Choose Tags..." required>
                        @foreach ($tags as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row" id="content-file-simple">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="checkbox-inline"><input type="checkbox" name="upload_mode" id="simple-mode" value="simple-mode">Simple mode</label>
                </div>
            </div>
        </div>
        <div class="row" id="content-file-simple-video">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Full Video :</label>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="form-group">
                    <video width="100%" controls style="margin-bottom:10px">
                        <source src="" id="prevfullvideosimple">
                        Your browser does not support HTML5 video.
                    </video>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="file" class="form-control" name="fullvideosimple" id="fullvideosimple" accept="video/*" required>
                    <span class="help-block">
                        <strong>Maximum file size : 700MB</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>File too large. Please change with smaller size.</strong>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" id="content-file">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Gambar Kecil :</label>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="form-group">
                    <img class="rounded" alt="200x200" id="thumbnailimage" width="100%" src="{{ asset('image/placeholder-image.png') }}" data-holder-rendered="true">
                </div>
            </div>
            <div class="col-md-6 text-left">
                <div class="form-group">
                    <input type="file" class="form-control" name="thumbnail" id="thumbnail" accept="image/*" required>
                    <span class="help-block">
                        <strong>Maksimal ukuran file : 700MB</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>Ukuran file terlalu besar !!</strong>
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Video Pendek :</label>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="form-group">
                    <video width="100%" controls style="margin-bottom:10px">
                        <source src="" id="prevshortvideo">
                        Browser anda tidak mendukung HTML5
                    </video>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <input type="file" class="form-control" name="shortvideo" id="shortvideo" accept="video/*" required>
                    <span class="help-block">
                        <strong>Ukuran file maksimal : 700MB</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>Ukuran file terlalu besar !!</strong>
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Video Penuh :</label>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="form-group">
                    <video width="100%" controls style="margin-bottom:10px">
                        <source src="" id="prevfullvideo">
                        Browser anda tidak mendukung HTML5
                    </video>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="file" class="form-control" name="fullvideo" id="fullvideo" accept="video/*" required>
                    <span class="help-block">
                        <strong>Ukuran file maksimal : 700MB</strong>
                    </span>
                    <span class="invalid-feedback" role="alert">
                        <strong>Ukuran file terlalu besar !!</strong>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" id="content-file-youtube">
             <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Youtube URL :</label>
                    <input type="url" class="form-control" name="urlyoutube" id="urlyoutube" accept="video/*" required>
                </div>
            </div>
        </div>
        <input type="hidden" value="video" name="category">
        <input type="hidden" id="method" name="method">
        <input type="hidden" id="id" name="id">
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
        <button type="submit"  class="btn btn-success waves-effect waves-light">Simpan</button>
    </div>
</form>
    
