<?php

namespace Modules\Video\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use App\Http\Model\Content;
use App\Http\Model\ContentDetail;
use App\Http\Model\Tags;
use App\Http\Model\Topics;
use App\Http\Model\ContentTags;
use App\Http\Model\Instansi;
use App\Http\Model\Category;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Validator;
use FFMpeg\FFMpeg;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Format\Video\X264;
use IDCT\Networking\Ssh\SftpClient;
use IDCT\Networking\Ssh\Credentials;
use File;
use Carbon\Carbon;
use Cohensive\Embed\Facades\Embed;


class VideoController extends Controller
{
    //view page
    public function index(Request $request)
    {
        $title = "VIDEO";
        $tags = Tags::get();
        $topics = Topics::get();
        if ($request->get('role') != "superadmin"){
            $instansi = Instansi::where('id', $request->get('instansi'))->get();
        }else{
            $instansi = Instansi::get();
        }
        $category = Category::where('group', 'video')->get();
        return view('video::index')
                ->withTopics($topics)
                ->withTags($tags)
                ->withInstansi($instansi)
                ->withCategory($category)
                ->withTitle($title);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = Content::where('category', '=', 'video');
        $data->with(['contentdetail','user','contenttags','topics', 'user.roles']);
        if ($request->get('role') != "superadmin"){
            $data->where('instansi_id', $request->get('instansi'))
                ->whereHas('user.roles', function($query){
                    $query->whereRaw('(roles.name = "admin")');
                });
        }
        $data->orderBy('content.created_at', 'DESC')->get();
        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {   
        // $client = new SftpClient();
        // $credentials = Credentials::withPassword('ftpuser', '$Allahuakbar123');
        // $client->setCredentials($credentials);
        // $client->connect(env('MEDIA_HOST'));

        if($request->method == 'update'){
            $data = $this->update($request->id, $request);
            return $data;
        }else if($request->method == 'content'){
            if (isset($request->upload_mode) && $request->upload_mode == "simple-mode"){
                $data = $this->updatecontentSimple($request->id, $request);
            }else{
                $data = $this->updatecontent($request->id, $request);
            }
            return $data;
        }else{
            if (isset($request->upload_mode) && $request->upload_mode == "simple-mode"){

                $model = new Content();
                $model->name = $request->name;
                $model->description = $request->description;
                $model->category = $request->category;
                $model->category_id = $request->category_id;
                $model->topics_id = $request->topics_id;
                $model->slug = $request->slug;
                $model->instansi_id = $request->instansi_id;
                $model->content_production = Carbon::createFromFormat('d-m-Y', $request->production_date ,'Asia/Jakarta')->format('Y/m/d');
                $model->user_id = Auth::user()->id;
                if(!$model->save()){
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
                    return json_encode($data);
                }else{
                    
                    if ($request->file('fullvideosimple')==null) {
                        
                        $urlyoutube = $request->urlyoutube;
                        $youtubeupload = $this->getVideoHtmlSource($urlyoutube);

                        if($youtubeupload){
                            $detail = new ContentDetail();
                            $detail->content_id = $model->id;
                            $detail->category = 'urlyoutube';
                            $detail->source = $youtubeupload;
                            $detail->save();
                            if (isset($request->tags)){
                                foreach($request->tags as $tags){
                                    $storetags = new ContentTags;
                                    $storetags->tags_id = $tags;
                                    $storetags->content_id = $model->id;
                                    $storetags->save(['timestamps' => false]);
                                }
                            }
                            $data = [
                                'status' => 1,
                                'message' => 'Success Update Data'
                            ];
                            return json_encode($data);
                        }else{
                            $data = [
                                'status' => 2,
                                'message' => 'Fail Update Data'
                            ];
                            return json_encode($data);
                        }
                    }

                    elseif ($request->file('fullvideosimple')!=null) {

                        $fullvideo = Storage::disk('sftp')->put('video/full', $request->file('fullvideosimple'));
                        $location = env('MEDIA_URL').$fullvideo;
                        
                        $thumbnail = $this->convertFrame($location, "thimbnail");
                        $thumbnailupload = $client->upload(public_path($thumbnail), '/usr/share/nginx/html/media/video/thumbnail/'.$thumbnail);
                        $deletefile = File::delete($thumbnail);
                        if ($thumbnailupload){
                            $detail = new ContentDetail();
                            $detail->content_id = $model->id;
                            $detail->category = 'thumbnail';
                            $detail->source = "video/thumbnail/".$thumbnail;
                            $detail->save();
                                
                            $shortvideo = $this->convertShortVideo($location, "shortvideo");
                            $shortvideoupload = $client->upload(public_path($shortvideo), '/usr/share/nginx/html/media/video/short/'.$shortvideo);
                            $deletefile = File::delete($shortvideo);

                            if ($shortvideoupload){

                                $detail = new ContentDetail();
                                $detail->content_id = $model->id;
                                $detail->category = 'shortvideo';
                                $detail->source = "video/short/".$shortvideo;
                                $detail->save();
    
                                $detail = new ContentDetail();
                                $detail->content_id = $model->id;
                                $detail->category = 'fullvideo';
                                $detail->source = $fullvideo;
                                $detail->save();
                                
                                if (isset($request->tags)){
                                    foreach($request->tags as $tags){
                                        $storetags = new ContentTags;
                                        $storetags->tags_id = $tags;
                                        $storetags->content_id = $model->id;
                                        $storetags->save(['timestamps' => false]);
                                    }
                                }
                                $data = [
                                    'status' => 1,
                                    'message' => 'Success Update Data'
                                ];
                                return json_encode($data);

                            }else{
                                $data = [
                                    'status' => 2,
                                    'message' => 'Fail Update Data'
                                ];
                                return json_encode($data);
                            }

                        }else{
                            $data = [
                                'status' => 2,
                                'message' => 'Fail Update Data'
                            ];
                            return json_encode($thumbnailupload); 
                        }
                    }else{

                        $data = [
                            'status' => 2,
                            'message' => 'Fail Update Data'
                        ];
                        return json_encode($data);   
                    }
                }
                return json_encode($data);

            }else{
                
                $validator = Validator::make($request->all(), [
                    'thumbnail' => 'required|file|max:100000000',
                    'shortvideo' => 'required|file|max:10000000000',
                    'fullvideo' => 'required|file|max:10000000000' // 2 MB
                ]);
    
                if ($validator->fails()){
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
    
                    return json_encode($data);
                }else{
                    $model = new Content();
                    $model->name = $request->name;
                    $model->description = $request->description;
                    $model->category = $request->category;
                    $model->category_id = $request->category_id;
                    $model->topics_id = $request->topics_id;
                    $model->slug = $request->slug;
                    $model->instansi_id = $request->instansi_id;
                    $model->content_production = Carbon::createFromFormat('d-m-Y', $request->production_date ,'Asia/Jakarta')->format('Y/m/d');
                    $model->user_id = Auth::user()->id;
                    if(!$model->save()){
                        $data = [
                            'status' => 2,
                            'message' => 'Fail Update Data'
                        ];
                        return json_encode($data);
                    }else{
                        $thumbnail = Storage::disk('sftp')->put('video/thumbnail', $request->file('thumbnail'));
                        if ($thumbnail) {
                            $detail = new ContentDetail();
                            $detail->content_id = $model->id;
                            $detail->category = 'thumbnail';
                            $detail->source = $thumbnail;
                            $detail->save();
                            $shortvideo = Storage::disk('sftp')->put('video/short', $request->file('shortvideo'));
                            if ($shortvideo) {
                                $detail = new ContentDetail();
                                $detail->content_id = $model->id;
                                $detail->category = 'shortvideo';
                                $detail->source = $shortvideo;
                                $detail->save();
                                $fullvideo = Storage::disk('sftp')->put('video/full', $request->file('fullvideo'));
                                if ($fullvideo) {
                                    $detail = new ContentDetail();
                                    $detail->content_id = $model->id;
                                    $detail->category = 'fullvideo';
                                    $detail->source = $fullvideo;
                                    $detail->save();
                                    if (isset($request->tags)){
                                        foreach($request->tags as $tags){
                                            $storetags = new ContentTags;
                                            $storetags->tags_id = $tags;
                                            $storetags->content_id = $model->id;
                                            $storetags->save(['timestamps' => false]);
                                        }
                                    }
                                    $data = [
                                        'status' => 1,
                                        'message' => 'Success Update Data'
                                    ];
                                    return json_encode($data);
                                }else{
                                    $data = [
                                        'status' => 2,
                                        'message' => 'Fail Update Data'
                                    ];
                                    return json_encode($data);
                                }
                            }else{
                                $data = [
                                    'status' => 2,
                                    'message' => 'Fail Update Data'
                                ];
                                return json_encode($data);
                            }
                        }else{
                            $data = [
                                'status' => 2,
                                'message' => 'Fail Update Data'
                            ];
                            return json_encode($data);
                        }
                    }
                }
            }
        }
        
    }

    //get data for Content
    public function content($id, Request $request)
    {
        $thumbnail = ContentDetail::where('content_id',$id)->where('category','thumbnail')->first();
        $shortvideo = ContentDetail::where('content_id',$id)->where('category','shortvideo')->first();
        $fullvideo = ContentDetail::where('content_id',$id)->where('category','fullvideo')->first();

        $data = [
            'id' => $id,
            'thumbnail' => $thumbnail->source,
            'shortvideo' => $shortvideo->source,
            'fullvideo' => $fullvideo->source,
            'urlyoutube' => $urlyoutube->source
        ];

        return json_encode($data);
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $content = Content::with(['contentdetail','contenttags'])->where('id',$id)->first();
        foreach ($content->contenttags as $key) {
            $tags[] = $key->tags_id;
        }
        $data = [
            'data' => $content,
            'tags' => $tags
        ];
        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = Content::findOrFail($id);
        $model->name = $request->name;
        $model->description = $request->description;
        $model->topics_id = $request->topics_id;
        $model->slug = $request->slug;
        $model->category_id = $request->category_id;
        $model->instansi_id = $request->instansi_id;
        $model->content_production = Carbon::createFromFormat('d-m-Y', $request->production_date ,'Asia/Jakarta')->format('Y/m/d');
        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
            return json_encode($data);
        }else{
            $tags = ContentTags::where('content_id',$id)->delete();
            foreach($request->tags as $tags){
                $storetags = new ContentTags;
                $storetags->tags_id = $tags;
                $storetags->content_id = $id;
                $storetags->save(['timestamps' => false]);
            }
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
            return json_encode($data);
        }
    }

    //update content
    public function updatecontent($id, Request $request)
    {
        if(!empty($request->thumbnail)){
            $thumbnail = ContentDetail::where('content_id',$id)->where('category','thumbnail')->first();
            $deletethumb = Storage::disk('sftp')->delete($thumbnail->source);
            $uploadthumb = Storage::disk('sftp')->put('video/thumbnail', $request->file('thumbnail'));
            $thumbnail->source = $uploadthumb;
            $thumbnail->update();
        }

        if(!empty($request->shortvideo)){
            $shortvideo = ContentDetail::where('content_id',$id)->where('category','shortvideo')->first();
            $deleteshort = Storage::disk('sftp')->delete($shortvideo->source);
            $uploadshort = Storage::disk('sftp')->put('video/short', $request->file('shortvideo'));
            $shortvideo->source = $uploadshort;
            $shortvideo->update();
        }

        if(!empty($request->fullvideo)){
            $fullvideo = ContentDetail::where('content_id',$id)->where('category','fullvideo')->first();
            $deletefull = Storage::disk('sftp')->delete($fullvideo->source);
            $uploadfull = Storage::disk('sftp')->put('video/full', $request->file('fullvideo'));
            $fullvideo->source = $uploadfull;
            $fullvideo->update();
        }

        if(!empty($request->urlyoutube)){
            $urlyoutube = ContentDetail::where('content_id',$id)->where('category','urlyoutube')->first();
            $deleteurl = delete($urlyoutube->source);
            $urlyoutube = $request->urlyoutube;
            $youtubeupload = $this->getVideoHtmlSource($urlyoutube);
            $urlyoutube->source = $youtubeupload;
            $urlyoutube->update();
        }

        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];

        return json_encode($data);
    }

    //update content
    public function updatecontentSimple($id, Request $request)
    {
        
        if(!empty($request->fullvideosimple)){
            // $client = new SftpClient();
            // $credentials = Credentials::withPassword('ftpuser', '$Allahuakbar123');
            // $client->setCredentials($credentials);
            // $client->connect(env('MEDIA_HOST'));

            $fullvideo = ContentDetail::where('content_id',$id)->where('category','fullvideo')->first();
            $deletefull = Storage::disk('sftp')->delete($fullvideo->source);
            $uploadfull = Storage::disk('sftp')->put('video/full', $request->file('fullvideosimple'));
            $fullvideo->source = $uploadfull;
            $fullvideo->update();
            
            $location = env('MEDIA_URL').$uploadfull;
            
            // Generate Shortvideo 
            $shortvideo = $this->convertShortVideo($location, "shortvideo");
            $shortvideoupload = $client->upload(public_path($shortvideo), '/usr/share/nginx/html/media/video/short/'.$shortvideo);
            $deletefile = File::delete($shortvideo);

            if ($shortvideoupload){
                $shortvideodata = ContentDetail::where('content_id',$id)->where('category','shortvideo')->first();
                $shortvideodata->source = "video/short/".$shortvideo;
                $shortvideodata->update();
            }

            // Generate Thumbnail
            $thumbnail = $this->convertFrame($location, "thimbnail");
            $thumbnailupload = $client->upload(public_path($thumbnail), '/usr/share/nginx/html/media/video/thumbnail/'.$thumbnail);
            $deletefile = File::delete($thumbnail);

            if ($thumbnailupload){
                $thumbnaildata = ContentDetail::where('content_id',$id)->where('category','thumbnail')->first();
                $thumbnaildata->source = "video/thumbnail/".$thumbnail;
                $thumbnaildata->update();
            }
        }

        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];

        return json_encode($data);
    }

    //approve data
    public function approve($id, Request $request)
    {
        $model = Content::findOrFail($id);
        $model->verified = 1;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //decline data
    public function decline($id, Request $request)
    {
        $model = Content::findOrFail($id);
        $model->verified = 0;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = Content::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $detail = ContentDetail::where('content_id',$id);
            foreach ($detail->get() as $del){
                $deletedata = Storage::disk('sftp')->delete($del->source);
            }
            $detail->delete();
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = Content::where('category', 'video')->get();

        $verified = 0;
        $unverified = 0;
        $total = 0;
        foreach($model as $key){
            if($key->verified){
                ++$verified;
            }else{
                ++$unverified;
            }
        }

        $info = [
            'totalverified' => $verified,
            'totalunverified' => $unverified,
            'total' => $verified+$unverified
        ];
        return json_encode($info);
    }

    public function convertFrame($location, $format){

        $ffmpeg = FFMpeg::create(array(
            'ffmpeg.binaries'  => env('FFMPEG_BIN'),
            'ffprobe.binaries' => env('FFPROB_BIN'),
            'timeout'          => 3600, // The timeout for the underlying process
            'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
        ));
        $video = $ffmpeg->open($location);
        
        $frame = $video->frame(TimeCode::fromSeconds(0));
        // $url = env('MEDIA_DIR').'video/thumbnail/'.$this->generateRandomString().'.jpg';
        $name = $this->generateRandomString().'.jpg';
        $frame->save($name);

        return $name;
    }

    public function convertShortVideo($location, $format){

        $ffmpeg = FFMpeg::create(array(
            'ffmpeg.binaries'  => env('FFMPEG_BIN'),
            'ffprobe.binaries' => env('FFPROB_BIN'),
            'timeout'          => 3600, // The timeout for the underlying process
            'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
        ));
        $video = $ffmpeg->open($location);
        
        $clip = $video->clip(TimeCode::fromSeconds(0), TimeCode::fromSeconds(5));
        // $url = env('MEDIA_DIR').'video/thumbnail/'.$this->generateRandomString().'.jpg';
        $name = $this->generateRandomString().'.mp4';
        $url = $clip->save(new X264('libmp3lame', 'libx264'),  $name);
        
        return $name;
    }

    public function generateRandomString($length = 20) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getVideoHtmlSource($urlyoutube)
    {
        $embed = Embed::make($urlyoutube)->parseUrl();

        if (!$embed)
            return '';
        
        $embed = $embed->getProvider()->render->iframe->src;

        return $embed;
    }
}
