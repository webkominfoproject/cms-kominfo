<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('video')->middleware('auth','userdata')->group(function() {
    Route::get('/', 'VideoController@index')->name('video.index');
    Route::get('data', 'VideoController@data')->name('video.data');
    Route::post('store', 'VideoController@store')->name('video.store');
    Route::get('edit/{id}', 'VideoController@edit')->name('video.edit');
    Route::get('content/{id}', 'VideoController@content')->name('video.content');
    Route::get('approve/{id}', 'VideoController@approve')->name('video.approve');
    Route::get('decline/{id}', 'VideoController@decline')->name('video.decline');
    Route::post('update/{id}', 'VideoController@update')->name('video.update');
    Route::get('destroy/{id}', 'VideoController@destroy')->name('video.destroy');
    Route::get('info', 'VideoController@info')->name('video.info');
});