<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('categoryfoto')->middleware('auth')->group(function() {
    Route::get('/', 'CategoryFotoController@index')->name('categoryfoto.index');
    Route::get('data', 'CategoryFotoController@data')->name('categoryfoto.data');
    Route::post('store', 'CategoryFotoController@store')->name('categoryfoto.store');
    Route::get('edit/{id}', 'CategoryFotoController@edit')->name('categoryfoto.edit');
    Route::post('update/{id}', 'CategoryFotoController@update')->name('categoryfoto.update');
    Route::get('destroy/{id}', 'CategoryFotoController@destroy')->name('categoryfoto.destroy');
    Route::get('info', 'CategoryFotoController@info')->name('categoryfoto.info');
    Route::get('approve/{id}', 'CategoryFotoController@approve')->name('categoryfoto.approve');
    Route::get('decline/{id}', 'CategoryFotoController@decline')->name('categoryfoto.decline');
});