<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Nama Konten</th>
        <th>Deskripsi Konten</th>
        <th>Tanggal Produksi</th>
        <th>Tanggal Upload</th>
        <th>Instansi</th>
        <th>Topik</th>
        <th>Kategori</th>
        <!-- <th>Tags</th> -->
        <th>Uploader</th>
        <th>Status</th>
        <th>Slug</th>
        <th>Jumlah Dilihat</th>
        <th>Jumlah Diunduh</th>
    </tr>
    </thead>
    <tbody>
    @foreach($listVideo as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->name }}</td>
            <td>{{ strip_tags($data->description) }}</td>
            <td>{{ (!empty($data->content_production)) ?  \Carbon\Carbon::createFromFormat('Y-m-d', $data->content_production ,'Asia/Jakarta')->format('d/M/Y') : "" }}</td>
            <td>{{ (!empty($data->created_at)) ?  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at ,'Asia/Jakarta')->format('d/M/Y') : "" }}</td>
            <td>{{ isset($data->instansi->name) ? $data->instansi->name : $data->instansi_id }}</td>
            <td>{{ $data->topics->name }}</td>
            <td>{{ isset($data->categoryrename->name) ?  $data->categoryrename->name : $data->category_id}}</td>
            <!-- <td>{{ $data->tags_id }}</td> -->
            <td>{{ $data->user->first_name." ".$data->user->last_name }}</td>
            <td>{{ ($data->verified == 0) ? "Tidak terverifikasi" : "Terverifikasi" }}</td>
            <td>{{ $data->slug }}</td>
            <td>{{ $data->memberactivitycountview_count }}</td>
            <td>{{ $data->memberactivitycountdownload_count }}</td>
        </tr>
    @endforeach
    </tbody>
</table>