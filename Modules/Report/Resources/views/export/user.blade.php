<table>
    <thead>
    <tr>
        <th>Nama</th>
        <th>NIK</th>
        <th>Email</th>
        <th>Instansi</th>
        <th>Jabatan</th>
        <th>Peran</th>
        <th>Status</th>
        <th>Tanggal Terdaftar</th>
    </tr>
    </thead>
    <tbody>
    @foreach($listUser as $data)
        <tr>
            <td>{{ $data->first_name." ".$data->last_name }}</td>
            <td>{{ $data->nik }}</td>
            <td>{{ $data->email }}</td>
            <td>{{ isset($data->instansi->name) ? $data->instansi->name : "-" }}</td>
            <td>{{ $data->jabatan }}</td>
            <td>{{ isset($data['roles'][0]) ?  $data['roles'][0]->name : "-" }}</td>
            <td>{{ ($data->status == 0) ? "Tidak terverifikasi" : "Terverifikasi" }}</td>
            <td>{{ (!empty($data->created_at)) ?  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at ,'Asia/Jakarta')->format('d M Y') : "" }}</td>
        </tr>
    @endforeach
    </tbody>
</table>