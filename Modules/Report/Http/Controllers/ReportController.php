<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Model\ContentStatus;
use App\Exports\VideoReport;
use App\Exports\FotoReport;
use App\Exports\ArtikelReport;
use App\Exports\UserReport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $title = "LAPORAN";
        $input = Input::get('type');
        
        if ($input ==  null){
            
            return view('report::index')->withTitle($title);
        }elseif($input ==  "video"){
            return Excel::download(new VideoReport, 'video.xlsx');
        }elseif($input ==  "artikel"){
            return Excel::download(new ArtikelReport, 'artikel.xlsx');
        }elseif($input ==  "foto"){
            return Excel::download(new FotoReport, 'foto.xlsx');
        }elseif($input == "user"){
            return Excel::download(new UserReport, 'pengguna.xlsx');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('report::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('report::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('report::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
