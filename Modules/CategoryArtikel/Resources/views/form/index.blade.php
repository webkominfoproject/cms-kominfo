<form method="POST" id="dataForm" action="#">
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama_client" class="control-label">Nama Kategori :</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nama_client" class="control-label">Deskripsi :</label>
                    <textarea  name="description" id="description-category" class="form-control" rows="3" required></textarea>
                </div>
                <div class="form-group">
                    <label class="control-label">Status :</label>
                    <select class="select2 form-control" name="status_kategori" id="status_kategori" data-placeholder="Pilih Status Kategori..." required>
                            <option value="aktif">Aktif</option>
                            <option value="nonaktif">Nonaktif</option>
                    </select>
                </div>
            </div>
            <input type="hidden" id="method">
            <input type="hidden" id="id">
            <input type="hidden" id="category-group" name="category_group">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
        <button type="submit"  class="btn btn-success waves-effect waves-light">Simpan</button>
    </div>
</form>
    
