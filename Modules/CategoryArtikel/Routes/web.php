<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('categoryartikel')->middleware('auth')->group(function() {
    Route::get('/', 'CategoryArtikelController@index')->name('categoryartikel.index');
    Route::get('data', 'CategoryArtikelController@data')->name('categoryartikel.data');
    Route::post('store', 'CategoryArtikelController@store')->name('categoryartikel.store');
    Route::get('edit/{id}', 'CategoryArtikelController@edit')->name('categoryartikel.edit');
    Route::post('update/{id}', 'CategoryArtikelController@update')->name('categoryartikel.update');
    Route::get('destroy/{id}', 'CategoryArtikelController@destroy')->name('categoryartikel.destroy');
    Route::get('info', 'CategoryArtikelController@info')->name('categoryartikel.info');
    Route::get('approve/{id}', 'CategoryArtikelController@approve')->name('categoryartikel.approve');
    Route::get('decline/{id}', 'CategoryArtikelController@decline')->name('categoryartikel.decline');
});