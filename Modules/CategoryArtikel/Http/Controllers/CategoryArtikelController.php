<?php

namespace Modules\CategoryArtikel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Guzzle;
use Yajra\Datatables\Datatables;
use App\Http\Model\Category;

class CategoryArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $title = 'KATEGORI';

        return view('categoryartikel::index')->withTitle($title);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('categoryartikel::create');
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = Category::where('group', 'artikel')->get(); 
        
        return Datatables::of($data)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $store = new Category();
        $store->name = $request->name;
        $store->status = $request->status_kategori;
        $store->group = $request->category_group;
        $store->description = $request->description;
        $store->save();
        
        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];
    
        return json_encode($data);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('categoryartikel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $data = Category::where('id', $id)->first();
        
        return json_encode($data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $model = Category::findOrFail($id);
        $model->name = $request->name;
        $model->status = $request->status_kategori;
        $model->description = $request->description;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $model = Category::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = Category::where('group', 'artikel')->get();
        $total = 0;
        foreach($model as $key){
            ++$total;
        }

        $info = [
            'total' => $total
        ];
        return json_encode($info);
    }

    //approve data
    public function approve($id, Request $request)
    {
        $model = Category::findOrFail($id);
        $model->status = "aktif";

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //decline data
    public function decline($id, Request $request)
    {
        $model = Category::findOrFail($id);
        $model->status = "nonaktif";

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }
}
