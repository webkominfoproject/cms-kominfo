<div class="card m-b-20">
    <div class="card-body">
        <div class="modal-body">
            <h4 class="page-title">Foto {{ $counter }}</h4>
            <div class="row" id="content-form">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nama_client" class="control-label">Nama Foto :</label>
                        <input type="text" name="namebulk[]" id="namebulk" class="namebulk form-control" required data-counter="{{$counter}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nama_client" class="control-label">Slug :</label>
                        <input type="text" name="slugbulk[]" id="slugbulk_{{$counter}}" class="slugbulk form-control" data-counter="{{$counter}}" required readonly>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nama_client" class="control-label">Deskripsi Foto :</label>
                        <textarea name="descriptionbulk[]" id="descriptionbulk" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Instansi :</label>
                        <select class="select2 form-control" name="instansi_id_bulk[]" id="instansi_id_bulk" data-placeholder="Pilih Instansi...">
                            @foreach ($instansi as $key)
                                <option value="{{ $key->id }}">{{ $key->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kategori :</label>
                        <select class="select2 form-control" name="category_idbulk[]" id="category_id" data-placeholder="Pilih Kategori..." required>
                            @foreach ($category as $key)
                                <option value="{{ $key->id }}">{{ $key->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Topik :</label>
                        <select class="select2 form-control" name="topics_id_bulk[]" id="topics_id_bulk" data-placeholder="Pilih Topik...">
                            @foreach ($topics as $key)
                                <option value="{{ $key->id }}">{{ $key->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tags :</label>
                        <select class="select2 form-control select2-multiple" name="tagsbulk_{{$counter}}[]" id="tagsbulk" multiple="multiple" multiple data-placeholder="Choose Tags..." required>
                            @foreach ($tags as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row" id="content-file">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nama_client" class="control-label">Foto :</label>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="form-group">
                        <img class="rounded" alt="200x200" id="fotoimagebulk" width="100%" src="{{ asset('image/placeholder-image.png') }}" data-holder-rendered="true">
                    </div>
                </div>
                <div class="col-md-6 text-left">
                    <div class="form-group">
                        <input type="file" class="form-control" name="fotobulk[]" id="fotobulk" accept="image/*" required>
                        <span class="help-block">
                            <strong>Ukuran file maksimal : 100MB</strong>
                        </span>
                        <span class="invalid-feedback" role="alert">
                            <strong>Ukuran file terlalu besar !!</strong>
                        </span>
                    </div>
                </div>
            </div>
            <!-- <input type="hidden" value="foto" name="category">
            <input type="hidden" id="method" name="method">
            <input type="hidden" id="id" name="id"> -->
        </div>
        <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Hapus</button> -->
            <!-- <button type="button" class="tambah-form btn btn-success waves-effect waves-light" data-dismiss="modal">Tambah</button> -->
            
        </div>
    </div>
</div>  
