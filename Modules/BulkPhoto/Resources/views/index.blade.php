@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Manajemen Konten
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <form method="POST" class="dataFormBulk" id="dataFormBulk" enctype='multipart/form-data'>
                {{ csrf_field() }}      
                <input type="hidden" value="foto" name="categorybulk">
                <input type="hidden" id="methodbulk" name="methodbulk" value="store">
                <div class="card m-b-20">
                    <div class="card-body">
                        <button type="button" class="tambah-form btn btn-success waves-effect waves-light" data-dismiss="modal">Tambah</button>
                        <button type="submit"  class="btn btn-warning waves-effect waves-light">Save</button>
                    </div>
                </div>
                <div id="ajax-bulk-image"></div> 
            </form>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('script')
<script type="text/javascript" src="http://malsup.github.com/jquery.form.js"></script>
<script type="text/javascript">
    var counter = 0;
    var baseURL = "{{url('')}}";
    var mediaURL = "{{ env('MEDIA_URL') }}";

    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '');
    }

    $( ".tambah-form" ).click(function() {
        counter++;

        $.ajax({
            type:'GET',
            url:'/bulkphoto/add-form',
            data:{counterParam: counter},
            success:function(r) {
                $( "#ajax-bulk-image" ).append( r );

                $(".namebulk").keyup(function(){
                    var Text = $(this).val();
                    var index = $(this).data("counter");
                    Text = convertToSlug(Text);
                    $("#slugbulk_"+index).val(Text);    
                });

                // tinymce.get('descriptionbulk').setContent('');
            }
        });
    });
    
    $('#dataFormBulk').ajaxForm({
        type: 'POST',
        url: baseURL+'/bulkphoto/upload',
        beforeSubmit : function(){
            $('#loader').css('display','block');
            $('.progress-bar').css('width','0%').text('0%');
            progressbar.iUploadHandle(true);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var persen = percentComplete + '%';
            $('.progress-bar').css('width',persen).text(persen);
            if(percentComplete == 100){
                $('.progress-bar').css('width',persen).text('processing');
            }
        },
        complete : function(xhr){
            $('#loader').css('display','none');
            var data = $.parseJSON(xhr.responseText);
            if (data.status == 2) {
                notification._toast('Terjadi kesalahan', data.message, 'error');
            }else{
                notification._toast('Update Data Berhasil', data.message, 'success');
            }
            progressbar.iUploadHandle(false);
            window.location.replace(baseURL+'/photo');
        }
    });
    
    // $(function () {
        $("#fotobulk").change(function () {
            var size = this.files[0].size;
            console.log(size);
            if(size > 100000000){
                $(this).addClass('is-invalid');
            }else{
                $(this).removeClass('is-invalid');
            }
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoadedBulk;
                reader.readAsDataURL(this.files[0]);
            }
        });
    // });

    function imageIsLoadedBulk(e) {
        $('#fotoimagebulk').attr('src', e.target.result);
    };

</script>
@endsection