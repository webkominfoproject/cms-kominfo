<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('bulkphoto')->group(function() {
    Route::get('/', 'BulkPhotoController@index')->name('bulkphoto.index');
    Route::post('/upload', 'BulkPhotoController@store');
    Route::get('/add-form', 'BulkPhotoController@addForm');
});
