<?php

namespace Modules\BulkPhoto\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Model\Content;
use App\Http\Model\ContentDetail;
use App\Http\Model\Tags;
use App\Http\Model\Topics;
use App\Http\Model\ContentTags;
use App\Http\Model\Instansi;
use App\Http\Model\Category;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;

class BulkPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $title = "Upload Banyak Foto";
        $tags = Tags::get();
        $topics = Topics::get();
        $instansi = Instansi::get();
        $category = Category::where('group', 'foto')->get();
        return view('bulkphoto::index')->withTags($tags)->withTopics($topics)->withTitle($title)->withInstansi($instansi)->withCategory($category);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bulkphoto::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        for ($i=0 ; $i < count($request->namebulk); $i++ ){
            $model = new Content();
            $model->name = $request->namebulk[$i];
            $model->description = $request->descriptionbulk[$i];
            $model->category = $request->categorybulk;
            $model->topics_id = $request->topics_id_bulk[$i];
            $model->category_id = $request->category_idbulk[$i];
            $model->instansi_id = $request->instansi_id_bulk[$i];
            $model->slug = $request->slugbulk[$i];
            $model->user_id = Auth::user()->id;
            if(!$model->save()){
                $data = [
                    'status' => 2,
                    'message' => 'Fail Update Data'
                ];
                return json_encode($data);
            }else{
                $foto = Storage::disk('sftp')->put('foto', $request->file('fotobulk')[$i]);
                if ($foto) {
                    $detail = new ContentDetail();
                    $detail->content_id = $model->id;
                    $detail->category = 'foto';
                    $detail->source = $foto;
                    $detail->save();
                    $index = $i+1;
                    if (isset($request->{'tagsbulk_'.(int)$index})){
                        foreach($request->{'tagsbulk_'.(int)$index} as $tags){
                            $storetags = new ContentTags;
                            $storetags->tags_id = $tags;
                            $storetags->content_id = $model->id;
                            $storetags->save(['timestamps' => false]);
                        }
                    }
                    $data = [
                        'status' => 1,
                        'message' => 'Success Update Data'
                    ];
                    $return = json_encode($data);
                }else{
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
                    $return = json_encode($data);
                }
            }
        }
        return json_encode($data);
    }
    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bulkphoto::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bulkphoto::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addForm(Request $request)
    {
        $title = "Bulk Photo";
        $tags = Tags::get();
        $topics = Topics::get();
        $instansi = Instansi::get();
        $category = Category::where('group', 'foto')->get();
        return view('bulkphoto::form.index')->withTags($tags)->withTopics($topics)->withTitle($title)->withCounter($_GET['counterParam'])->withInstansi($instansi)->withCategory($category);
    }
}
