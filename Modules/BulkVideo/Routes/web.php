<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('bulkvideo')->group(function() {
    Route::get('/', 'BulkVideoController@index')->name('bulkvideo.index');
    Route::post('/upload', 'BulkVideoController@store');
    Route::get('/add-form', 'BulkVideoController@addForm');
});
