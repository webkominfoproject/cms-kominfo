<div class="card m-b-20">
    <div class="card-body">
        <div class="modal-body">
            <h4 class="page-title">Video {{ $counter }}</h4>
                <div class="row" id="content-form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Nama Video :</label>
                            <input type="text" name="namebulk[]" id="namebulk" class="namebulk form-control" data-counter="{{$counter}}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Slug :</label>
                            <input type="text" name="slugbulk[]" id="slugbulk_{{$counter}}" class="slugbulk form-control" data-counter="{{$counter}}" required readonly>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Deskripsi Video :</label>
                            <textarea name="descriptionbulk[]" id="descriptionbulk" class="descriptionbulk form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kategori :</label>
                            <select class="select2 form-control" name="category_idbulk[]" id="category_id" data-placeholder="Pilih Kategori..." required>
                                @foreach ($category as $key)
                                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Instansi :</label>
                            <select class="select2 form-control" name="instansi_idbulk[]" id="instansi_idbulk" data-placeholder="Choose Topics..." required>
                                @foreach ($instansi as $key)
                                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Topik :</label>
                            <select class="select2 form-control" name="topics_idbulk[]" id="topics_idbulk" data-placeholder="Choose Topics..." required>
                                @foreach ($topics as $key)
                                    <option value="{{ $key->id }}">{{ $key->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tags :</label>
                            <select class="select2 form-control select2-multiple" name="tagsbulk_{{ $counter }}[]" id="tagsbulk" multiple="multiple" multiple data-placeholder="Choose Tags..." required>
                                @foreach ($tags as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" id="content-file">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Gambar Kecil :</label>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="form-group">
                            <img class="rounded" alt="200x200" id="thumbnailimage" width="100%" src="{{ asset('image/placeholder-image.png') }}" data-holder-rendered="true">
                        </div>
                    </div>
                    <div class="col-md-6 text-left">
                        <div class="form-group">
                            <input type="file" class="form-control" name="thumbnailbulk[]" id="thumbnailbulk" accept="image/*" required>
                            <span class="help-block">
                                <strong>Ukuran file maksimal : 700MB</strong>
                            </span>
                            <span class="invalid-feedback" role="alert">
                                <strong>Ukuran file terlalu besar !!</strong>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Video Pendek :</label>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="form-group">
                            <video width="100%" controls style="margin-bottom:10px">
                                <source src="" id="prevshortvideo">
                                Browser anda tidak mendukung HTML5
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <input type="file" class="form-control" name="shortvideobulk[]" id="shortvideobulk" accept="video/*" required>
                            <span class="help-block">
                                <strong>Ukuran file maksimal : 700MB</strong>
                            </span>
                            <span class="invalid-feedback" role="alert">
                                <strong>Ukuran file terlalu besar !!</strong>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_client" class="control-label">Video Penuh :</label>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="form-group">
                            <video width="100%" controls style="margin-bottom:10px">
                                <source src="" id="prevfullvideo">
                                Browser anda tidak mendukung HTML5
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="file" class="form-control" name="fullvideobulk[]" id="fullvideobulk" accept="video/*" required>
                            <span class="help-block">
                                <strong>Ukuran file maksimal : 700MB</strong>
                            </span>
                            <span class="invalid-feedback" role="alert">
                                <strong>Ukuran file terlalu besar !!</strong>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- <input type="hidden" value="video" name="category">
                <input type="hidden" id="method" name="method">
                <input type="hidden" id="id" name="id"> -->
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="submit"  class="btn btn-success waves-effect waves-light">Save</button>
            </div> -->
        </div>
    </div>
</div>
    
