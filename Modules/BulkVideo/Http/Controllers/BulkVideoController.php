<?php

namespace Modules\BulkVideo\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Model\Tags;
use App\Http\Model\Topics;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Model\Content;
use App\Http\Model\ContentDetail;
use App\Http\Model\ContentTags;
use App\Http\Model\Instansi;
use App\Http\Model\Category;
use Illuminate\Support\Facades\Storage;

class BulkVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $title = "VIDEO";
        $tags = Tags::get();
        $topics = Topics::get();
        $instansi = Instansi::get();
        $category = Category::where('group', 'video')->get();
        return view('bulkvideo::index')->withTags($tags)->withTopics($topics)->withTitle($title)->withInstansi($instansi)->withCategory($category);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bulkvideo::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // return json_encode($request->all());
        for ($i=0 ; $i < count($request->namebulk); $i++ ){

            $model = new Content();
            $model->name = $request->namebulk[$i];
            $model->description = $request->descriptionbulk[$i];
            $model->category = $request->categorybulk;
            $model->category_id = $request->category_idbulk[$i];
            $model->topics_id = $request->topics_idbulk[$i];
            $model->instansi_id = $request->instansi_idbulk[$i];
            $model->slug = $request->slugbulk[$i];
            $model->user_id = Auth::user()->id;
            if(!$model->save()){
                $data = [
                    'status' => 2,
                    'message' => 'Fail Update Data'
                ];
                return json_encode($data);
            }else{
                $thumbnail = Storage::disk('sftp')->put('video/thumbnail', $request->file('thumbnailbulk')[$i]);
                if ($thumbnail) {
                    $detail = new ContentDetail();
                    $detail->content_id = $model->id;
                    $detail->category = 'thumbnail';
                    $detail->source = $thumbnail;
                    $detail->save();
                    $shortvideo = Storage::disk('sftp')->put('video/short', $request->file('shortvideobulk')[$i]);
                    if ($shortvideo) {
                        $detail = new ContentDetail();
                        $detail->content_id = $model->id;
                        $detail->category = 'shortvideo';
                        $detail->source = $shortvideo;
                        $detail->save();
                        $fullvideo = Storage::disk('sftp')->put('video/full', $request->file('fullvideobulk')[$i]);
                        if ($fullvideo) {
                            $detail = new ContentDetail();
                            $detail->content_id = $model->id;
                            $detail->category = 'fullvideo';
                            $detail->source = $fullvideo;
                            $detail->save();
                            $index = $i+1;
                            if (isset($request->{'tagsbulk_'.(int)$index})){
                                foreach($request->{'tagsbulk_'.(int)$index} as $tags){
                                    $storetags = new ContentTags;
                                    $storetags->tags_id = $tags;
                                    $storetags->content_id = $model->id;
                                    $storetags->save(['timestamps' => false]);
                                }
                            }
                            $data = [
                                'status' => 1,
                                'message' => 'Success Update Data'
                            ];
                            // return json_encode($data);
                        }else{
                            $data = [
                                'status' => 2,
                                'message' => 'Fail Update Data'
                            ];
                            return json_encode($data);
                        }
                    }else{
                        $data = [
                            'status' => 2,
                            'message' => 'Fail Update Data'
                        ];
                        return json_encode($data);
                    }
                }else{
                    $data = [
                        'status' => 2,
                        'message' => 'Fail Update Data'
                    ];
                    return json_encode($data);
                }
            }
        }
        return json_encode($data);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bulkvideo::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bulkvideo::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function addForm(Request $request)
    {
        $title = "Bulk Video";
        $tags = Tags::get();
        $topics = Topics::get();
        $instansi = Instansi::get();
        $category = Category::where('group', 'video')->get();
        return view('bulkvideo::form.index')->withTags($tags)->withTopics($topics)->withTitle($title)->withCounter($_GET['counterParam'])->withInstansi($instansi)->withCategory($category);
    }
}
