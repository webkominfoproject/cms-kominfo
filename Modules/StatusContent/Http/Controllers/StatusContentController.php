<?php

namespace Modules\StatusContent\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Model\ContentStatus;

class StatusContentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $title = "KONTEN MANAJEMEN";
        $home = [ 
            'fokus_status' => ContentStatus::where('name', 'fokus')->first(),
            'video_status' => ContentStatus::where('name', 'video')->first(),
            'foto_status' => ContentStatus::where('name', 'foto')->first(),
            'artikel_status' => ContentStatus::where('name', 'artikel')->first(),
        ];

        return view('statuscontent::index')->withHome($home)->withTitle($title);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('statuscontent::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //home
        $data = ContentStatus::count();
        if ($data >= 4) {
            $fokus = ContentStatus::where('name','fokus')->first();
            $fokus->status = isset($request->fokus_status) ? "aktif" : "nonaktif";
            $fokus->update();

            $video = ContentStatus::where('name','video')->first();
            $video->status = isset($request->video_status) ? "aktif" : "nonaktif";
            $video->update();

            $foto = ContentStatus::where('name','foto')->first();
            $foto->status = isset($request->foto_status) ? "aktif" : "nonaktif";
            $foto->update();

            $artikel = ContentStatus::where('name','artikel')->first();
            $artikel->status = isset($request->artikel_status) ? "aktif" : "nonaktif";
            $artikel->update();

            return redirect()->route('statuscontent.index');
        }else{
            $fokus = new ContentStatus();
            $fokus->name = 'fokus';
            $fokus->status = isset($request->fokus_status) ? "aktif" : "nonaktif";
            $fokus->save();

            $video = new ContentStatus();
            $video->name = 'video';
            $video->status = isset($request->video_status) ? "aktif" : "nonaktif";
            $video->save();

            $foto = new ContentStatus();
            $foto->name = 'foto';
            $foto->status = isset($request->foto_status) ? "aktif" : "nonaktif";
            $foto->save();

            $artikel = new ContentStatus();
            $artikel->name = 'artikel';
            $artikel->status = isset($request->artikel_status) ? "aktif" : "nonaktif";
            $artikel->save();

            return redirect()->route('statuscontent.index');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('statuscontent::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('statuscontent::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
