<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('statuscontent')->middleware('auth')->group(function() {
    Route::get('/', 'StatusContentController@index')->name('statuscontent.index');
    Route::get('data', 'StatusContentController@data')->name('statuscontent.data');
    Route::post('store', 'StatusContentController@store')->name('statuscontent.store');
    Route::get('edit/{id}', 'StatusContentController@edit')->name('statuscontent.edit');
    Route::get('content/{id}', 'StatusContentController@content')->name('statuscontent.content');
    Route::get('approve/{id}', 'StatusContentController@approve')->name('statuscontent.approve');
    Route::get('decline/{id}', 'StatusContentController@decline')->name('statuscontent.decline');
    Route::post('update/{id}', 'StatusContentController@update')->name('statuscontent.update');
    Route::get('destroy/{id}', 'StatusContentController@destroy')->name('statuscontent.destroy');
    Route::get('info', 'StatusContentController@info')->name('statuscontent.info');
    Route::get('/add-form', 'StatusContentController@addForm');
});