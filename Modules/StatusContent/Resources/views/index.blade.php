@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ $title }}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Manajemen Website
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
            	<form action="{{ route('statuscontent.store') }}" method="POST">
            		{{ csrf_field() }}
            		<input type="hidden" name="pages" value="home">
	            	<div class="card-header">
	                	<div class="row">
		                    <div class="col-md-6 text-left">
		                        <h4 class="mt-10 m-b-10 header-title"></h4>
		                    </div>
		                    <div class="col-md-6 text-right">
		                        <button type="submit" class="btn btn-rounded btn-success">
			                        <span>
			                            <i class="ti-reload"></i> Terapkan
			                        </span>
		                        </button>
		                    </div>
		                </div>
	                </div>
                	<div class="card-body">
	                    <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Fokus </label>
                            <div class="col-sm-10">
                                <input type="checkbox" data-on="Aktif" data-off="Nonaktif" <?php echo ($home['fokus_status']['status'] != "aktif") ? "" : "checked"; ?> data-toggle="toggle" name="fokus_status">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Video </label>
                            <div class="col-sm-10">
                            <input type="checkbox" data-on="Aktif" data-off="Nonaktif" <?php echo ($home['video_status']['status'] != "aktif") ? "" : "checked"; ?> data-toggle="toggle" name="video_status">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Foto </label>
                            <div class="col-sm-10">
                            <input type="checkbox" data-on="Aktif" data-off="Nonaktif" <?php echo ($home['foto_status']['status'] != "aktif") ? "" : "checked"; ?> data-toggle="toggle" name="foto_status">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-sm-2 col-form-label">Artikel </label>
                            <div class="col-sm-10">
                            <input type="checkbox" data-on="Aktif" data-off="Nonaktif" <?php echo ($home['artikel_status']['status'] != "aktif") ? "" : "checked"; ?> data-toggle="toggle" name="artikel_status">
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
@endsection
@section('script')
<script type="text/javascript">

    $(document).ready(function() {
        app.init();
        $('.select2').select2();
    })

</script>
@endsection