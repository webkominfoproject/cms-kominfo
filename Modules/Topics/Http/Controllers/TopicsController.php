<?php

namespace Modules\Topics\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Guzzle;
use Yajra\Datatables\Datatables;
use App\Http\Model\Topics;

class TopicsController extends Controller
{
    //view page
    public function index()
    {

        $title = 'TOPICS';

        return view('topics::index')->withTitle($title);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = Topics::get(); 
        
        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        $store = new Topics();
        $store->name = $request->name;
        $store->save();
        
        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];
    
        return json_encode($data);
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $data = Topics::where('id', $id)->first();
        
        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = Topics::findOrFail($id);
        $model->name = $request->name;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = Topics::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = Topics::get();
        $total = 0;
        foreach($model as $key){
            ++$total;
        }

        $info = [
            'total' => $total
        ];
        return json_encode($info);
    }
}
