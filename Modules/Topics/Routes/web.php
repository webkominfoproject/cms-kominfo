<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('topics')->middleware('auth')->group(function() {
    Route::get('/', 'TopicsController@index')->name('topics.index');
    Route::get('data', 'TopicsController@data')->name('topics.data');
    Route::post('store', 'TopicsController@store')->name('topics.store');
    Route::get('edit/{id}', 'TopicsController@edit')->name('topics.edit');
    Route::post('update/{id}', 'TopicsController@update')->name('topics.update');
    Route::get('destroy/{id}', 'TopicsController@destroy')->name('topics.destroy');
    Route::get('info', 'TopicsController@info')->name('topics.info');
});
