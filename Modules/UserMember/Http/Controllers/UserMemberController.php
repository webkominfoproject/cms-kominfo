<?php

namespace Modules\UserMember\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Guzzle;
use App\User;
use Auth;
use Validator;
use Storage;
use App\Http\Model\Instansi;
use Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;

class UserMemberController extends Controller
{
    //view page
    public function index()
    {
        $data['role'] = Role::get();
        $data['instansi'] = Instansi::get();
        $user = User::with(['roles','instansi'])->get()->toArray();
        

        foreach ($user as $key) {
            if (empty($key['roles'])) {
                $datas[] = $key;
            }
        }
        return view('usermember::index')->withData($data);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = User::with(['roles','instansi'])
            ->whereHas('roles', function($query){
                $query->where('name', 'member');
            })
            ->where('id','!=',Auth::user()->id)
            ->get();
        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        $model = new User();
        $model->first_name = $request->first_name;
        $model->last_name = $request->last_name;
        $model->instansi_id = $request->instansi_id;
        $model->jabatan = $request->jabatan;
        $model->nik = $request->nik;
        $model->email = $request->email;
        $model->password = bcrypt($request->password);

        if(!$model->save()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
            // if($request->role != 'Member'){
            $model->assignRole($request->role);
            // }
        }
    
        return json_encode($data);
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $data = User::with(['roles','instansi'])->where('id',$id)
            ->whereHas('roles', function($query){
                $query->where('name', 'member');
            })->first();

        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = User::findOrFail($id);
        $model->first_name = $request->first_name;
        $model->last_name = $request->last_name;
        $model->instansi_id = $request->instansi_id;
        $model->jabatan = $request->jabatan;
        $model->nik = $request->nik;
        $model->email = $request->email;
        // $model->password = bcrypt($request->password);

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
            // if($request->role != 'Member'){
            $model->syncRoles([$request->role]);
            // }else{
                // $model->syncRoles(['']);
            // }
        }

        return json_encode($data);
    }

    //approve data
    public function approve($id, Request $request)
    {
        $model = User::findOrFail($id);
        $model->status = 1;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //decline data
    public function decline($id, Request $request)
    {
        $model = User::findOrFail($id);
        $model->status = 0;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = User::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = User::get();

        $verified = 0;
        $unverified = 0;
        $total = 0;
        foreach($model as $key){
            if($key->status){
                ++$verified;
            }else{
                ++$unverified;
            }
        }

        $info = [
            'totalverified' => $verified,
            'totalunverified' => $unverified,
            'total' => $verified+$unverified
        ];
        return json_encode($info);
    }

    //profil 
    public function profil()
    {
        $user = User::findOrFail(Auth::user()->id);
        $instansi = Instansi::get();
        $title = 'PROFIL';
        return view('user::profil')->withInstansi($instansi)->withData($user)->withTitle($title);
    }

    public function updateprofil(Request $request)
    {
        $model = User::findOrFail($request->id);
        $model->first_name = $request->first_name;
        $model->last_name = $request->last_name;
        $model->instansi_id = $request->instansi_id;
        $model->jabatan = $request->jabatan;
        $model->nik = $request->nik;
        $model->email = $request->email;
        if($request->new_password != null){
            $model->password = bcrypt($request->new_password);
        }

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $request->session()->regenerate();
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    public function updateprofilpic(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto' => 'required|file|max:100000000',
        ]);

        if ($validator->fails()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];

            return json_encode($data);
        }else{
            $foto = Storage::disk('sftp')->put('foto', $request->file('foto'));
            if ($foto) {
                $model = User::findOrFail($request->id);
                $model->image = $foto;
                $model->update();
                $request->session()->regenerate();
                $data = [
                    'status' => 1,
                    'message' => 'Success Update Data'
                ];
                return json_encode($data);
            }else{
                $data = [
                    'status' => 2,
                    'message' => 'Fail Update Data'
                ];
                return json_encode($data);
            }
        }
    }

    public function deletepic(Request $request)
    {
        $model = User::findOrFail($request->id);
        $model->image = null;
        $model->update();
        $request->session()->regenerate();
        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];
        return redirect()->route('profil.index');
    }
}
