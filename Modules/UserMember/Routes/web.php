<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('usermember')->group(function() {
    Route::get('/', 'UserMemberController@index')->name('usermember.index');
    Route::get('data', 'UserMemberController@data')->name('usermember.data');
    Route::post('store', 'UserMemberController@store')->name('usermember.store');
    Route::get('edit/{id}', 'UserMemberController@edit')->name('usermember.edit');
    Route::post('update/{id}', 'UserMemberController@update')->name('usermember.update');
    Route::get('approve/{id}', 'UserMemberController@approve')->name('usermember.approve');
    Route::get('decline/{id}', 'UserMemberController@decline')->name('usermember.decline');
    Route::get('destroy/{id}', 'UserMemberController@destroy')->name('usermember.destroy');
    Route::get('info', 'UserMemberController@info')->name('usermember.info');
});