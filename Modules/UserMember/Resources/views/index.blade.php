@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">PENGGUNA</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        Manajemen Pengguna
                    </li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    <div class="state-graph">
                            <div class="text-danger" style="font-size: 20pt;margin-bottom: -5px;" id="totalunverified"></div>
                            <div class="info text-danger">Pengguna non-aktif</div>
                    </div>
                    <div class="state-graph">
                        <div class="text-success" style="font-size: 20pt;margin-bottom: -5px;" id="totalverified"></div>
                        <div class="info text-success">Pengguna aktif</div>
                    </div>
                    <div class="state-graph">
                        <div style="font-size: 20pt;margin-bottom: -5px;color:black" id="total"></div>
                        <div class="info">Jumlah pengguna</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-6 text-left">
                        <h4 class="mt-0 m-b-30 header-title">Daftar Pengguna</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="pull-right" href="#" id="add-data">
                        <span class="btn btn-rounded btn-success">
                            <i class="ti-plus"></i> Tambah Pengguna
                        </span>
                        </a>
                    </div>
                    </div>
                    <table id="dataTable" class="table table-responsive-sm table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Depan</th>
                            <th>Nama Belakang</th>
                            <th>Instansi</th>
                            <th>Email</th>
                            <th>Peran</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        
    </div>
    <!-- end row -->

</div> <!-- end container-fluid -->
@endsection
@section('data-content')
    @include('usermember::form.user')
@endsection

@section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                app.handleUserMemberPage();
                $('.select2').select2();
            })
        </script>
        <script>
    $(window).bind("resize", function () {
        console.log($(this).width())
        if ($(this).width() < 768) {
            $('table').removeClass('dt-responsive')
        }
    }).trigger('resize');
</script>
@endsection