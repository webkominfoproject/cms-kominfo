<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('photo')->middleware('auth', 'userdata')->group(function() {
    Route::get('/', 'PhotoController@index')->name('photo.index');
    Route::get('data', 'PhotoController@data')->name('photo.data');
    Route::post('store', 'PhotoController@store')->name('photo.store');
    Route::get('edit/{id}', 'PhotoController@edit')->name('photo.edit');
    Route::get('content/{id}', 'PhotoController@content')->name('photo.content');
    Route::get('approve/{id}', 'PhotoController@approve')->name('photo.approve');
    Route::get('decline/{id}', 'PhotoController@decline')->name('photo.decline');
    Route::post('update/{id}', 'PhotoController@update')->name('photo.update');
    Route::get('destroy/{id}', 'PhotoController@destroy')->name('photo.destroy');
    Route::get('info', 'PhotoController@info')->name('photo.info');
});