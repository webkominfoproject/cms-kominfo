<?php

namespace Modules\Tags\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Guzzle;
use Yajra\Datatables\Datatables;
use App\Http\Model\Tags;

class TagsController extends Controller
{
    //view page
    public function index()
    {
        $title = 'TAGS';
        return view('tags::index')->withTitle($title);
    }

    //get data fot DataTable
    public function data(Request $request)
    {
        $data = Tags::get();

        return Datatables::of($data)->make(true);
    }

    //store data
    public function store(Request $request)
    {
        $store = new Tags();
        $store->name = $request->name;
        $store->save();
       
        $data = [
            'status' => 1,
            'message' => 'Success Update Data'
        ];
    
        return json_encode($data);
    }

    //get data for Edit
    public function edit($id, Request $request)
    {
        $data = Tags::where('id', $id)->first();
        
        return json_encode($data);
    }

    //update data
    public function update($id, Request $request)
    {
        $model = Tags::findOrFail($id);
        $model->name = $request->name;

        if(!$model->update()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //delete data
    public function destroy($id, Request $request)
    {
        $model = Tags::where('id', $id);

        if(!$model->delete()){
            $data = [
                'status' => 2,
                'message' => 'Fail Update Data'
            ];
        }else{
            $data = [
                'status' => 1,
                'message' => 'Success Update Data'
            ];
        }

        return json_encode($data);
    }

    //get info data
    public function info(Request $request)
    {
        $model = Tags::get();
        $total = 0;
        foreach($model as $key){
            ++$total;
        }

        $info = [
            'total' => $total
        ];
        return json_encode($info);
    }
}
