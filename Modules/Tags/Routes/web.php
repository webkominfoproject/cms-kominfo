<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('tags')->middleware('auth')->group(function() {
    Route::get('/', 'TagsController@index')->name('tags.index');
    Route::get('data', 'TagsController@data')->name('tags.data');
    Route::post('store', 'TagsController@store')->name('tags.store');
    Route::get('edit/{id}', 'TagsController@edit')->name('tags.edit');
    Route::post('update/{id}', 'TagsController@update')->name('tags.update');
    Route::get('destroy/{id}', 'TagsController@destroy')->name('tags.destroy');
    Route::get('info', 'TagsController@info')->name('tags.info');
});
