<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Helpers\Guzzle;

class AuthenticatedUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('user')){
            // check if in API still login or not
            $now = Carbon::now()->timestamp;

            if($now <= $request->session()->get('expired_token')){
                $request->session()->forget('expired_token');
                $request->session()->forget('token_user');
                $request->session()->forget('user');

                return redirect(route('index'));
            }

            return $next($request);
        }else{
            return redirect(route('index'));
        }
    }
}

