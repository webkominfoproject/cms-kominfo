<?php

namespace App\Http\Middleware;

use Closure;
use View;
use App\Http\Helpers\Guzzle;

class AuthOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('user')){
            return redirect()->route('dashboard.index');
        } else return $next($request);
    }
}