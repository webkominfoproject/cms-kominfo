<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class GetUserData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $role = $user->roles->first()->name;
        $instansi =$user->instansi_id;

        $request->attributes->add([
            'role' => $role,
            'instansi' => $instansi,
        ]);

        return $next($request);
    }
}
