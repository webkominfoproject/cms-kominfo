<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
	use SoftDeletes;

	//nama tabel
	protected $table = 'news';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //atribut soft delete
    protected $dates = ['deleted_at'];

    //relasi
    public function topics()
    {
        return $this->belongsTo(\App\Http\Model\Topics::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
