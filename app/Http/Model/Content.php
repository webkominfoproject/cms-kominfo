<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
	use SoftDeletes;

	//nama tabel
	protected $table = 'content';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //atribut soft delete
    protected $dates = ['deleted_at'];

    //relasi
    public function contentdetail()
    {
        return $this->hasMany(\App\Http\Model\ContentDetail::class);
    }

    public function contenttags()
    {
        return $this->hasMany(\App\Http\Model\ContentTags::class);
    }

    public function memberactivity()
    {
        return $this->hasMany(\App\Http\Model\MemberActivity::class);
    }

    public function memberactivitycountview()
    {
        return $this->hasMany(\App\Http\Model\MemberActivity::class)->where('activity', 'view');
    }

    public function memberactivitycountdownload()
    {
        return $this->hasMany(\App\Http\Model\MemberActivity::class)->where('activity', 'download');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function topics()
    {
        return $this->belongsTo(\App\Http\Model\Topics::class);
    }

    public function category()
    {
        return $this->belongsTo(\App\Http\Model\Category::class);
    }

    public function instansi()
    {
        return $this->belongsTo(\App\Http\Model\Instansi::class);
    }

    public function categoryrename()
    {
        return $this->belongsTo(\App\Http\Model\CategoryRename::class, 'category_id');
    }
}
