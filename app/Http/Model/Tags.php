<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tags extends Model
{
	use SoftDeletes;

	//nama tabel
	protected $table = 'tags';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //atribut soft delete
    protected $dates = ['deleted_at'];

    //relasi
    public function contenttags()
    {
        return $this->hasMany(\App\Http\Model\ContentTags::class);
    }
}
