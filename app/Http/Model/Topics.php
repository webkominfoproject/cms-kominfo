<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topics extends Model
{
	use SoftDeletes;

	//nama tabel
	protected $table = 'topics';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //atribut soft delete
    protected $dates = ['deleted_at'];

    //relasi
    public function content()
    {
        return $this->hasMany(\App\Http\Model\Content::class);
    }

    public function blog()
    {
        return $this->hasMany(\App\Http\Model\Blog::class);
    }
}
