<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentStatus extends Model
{
	//nama tabel
	protected $table = 'content_status';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

}
