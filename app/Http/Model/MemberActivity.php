<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberActivity extends Model
{
	//nama tabel
	protected $table = 'member_activity';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //relasi
    public function content()
    {
        return $this->belongsTo(\App\Http\Model\Content::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

}
