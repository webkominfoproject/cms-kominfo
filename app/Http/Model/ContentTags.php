<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentTags extends Model
{
	//nama tabel
	protected $table = 'content_tags';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    public $timestamps = false;
    
    //relasi
    public function content()
    {
        return $this->belongsTo(\App\Http\Model\Content::class);
    }

    public function tags()
    {
        return $this->belongsTo(\App\Http\Model\Tags::class);
    }
}
