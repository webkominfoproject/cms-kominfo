<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instansi extends Model
{
	use SoftDeletes;

	//nama tabel
	protected $table = 'instansi';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //atribut soft delete
    protected $dates = ['deleted_at'];

    //relasi
    public function user()
    {
        return $this->hasMany(\App\User::class);
    }

    public function content()
    {
        return $this->hasMany(\App\Http\Model\Content::class);
    }
}
