<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentDetail extends Model
{
	//nama tabel
	protected $table = 'content_detail';

    //atribut yang terpengaruh
    protected $fillable = [
        '*'
    ];

    //relasi
    public function content()
    {
        return $this->belongsTo(\App\Http\Model\Content::class);
    }

}
