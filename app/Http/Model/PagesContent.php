<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PagesContent extends Model
{
	use SoftDeletes;

	//nama tabel
	protected $table = 'pages_content';

    //atribut yang terpengaruh
    protected $fillable = [
        'pages','section','content'
    ];

    //atribut soft delete
    protected $dates = ['deleted_at'];

    //relasi
}
