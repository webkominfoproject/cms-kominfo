<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use App\Http\Model\Content;

class FotoReport implements FromView
{
    public function view(): \Illuminate\Contracts\View\View
    {
        // dd(Content::withCount(['memberactivitycountview', 'memberactivitycountdownload'])->with(['instansi', 'user', 'topics', 'category'])->where("category", "video")->where("id", "162")->get());
        
        return view('report::export.video', [
            'listVideo' => Content::withCount(['memberactivitycountview', 'memberactivitycountdownload'])->with(['instansi', 'user', 'topics', 'categoryrename', 'topics'])->where("category", "foto")->get()
        ]);
    }
}
