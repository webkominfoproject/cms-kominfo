<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use App\User;

class UserReport implements FromView
{
    public function view(): \Illuminate\Contracts\View\View
    {
        // $a = User::with(['roles','instansi'])->get();
        // dd($a);
        return view('report::export.user', [
            'listUser' => User::with(['roles','instansi'])->get()
        ]);
    }
}
