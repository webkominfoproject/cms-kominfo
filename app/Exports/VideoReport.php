<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use App\Http\Model\Content;

class VideoReport implements FromView
{
    public function view(): \Illuminate\Contracts\View\View
    {
        // $a = Content::withCount(['memberactivitycountview', 'memberactivitycountdownload'])->with(['instansi', 'user', 'topics', 'categoryrename'])->where("category", "video")->where("id", "162")->get();
        // dd($a);
        return view('report::export.video', [
            'listVideo' => Content::withCount(['memberactivitycountview', 'memberactivitycountdownload'])->with(['instansi', 'user', 'topics', 'categoryrename', 'topics'])->where("category", "video")->get()
        ]);
    }
}
