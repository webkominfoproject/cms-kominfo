**How to deploy application :**

1. **Clone** repo using **Git**
2. Create **.env** files, then copy data from **Google Docs Documentation**
3. Run **"composer update"**
4. Import database from **"db_kom.sql"**
5. Run **"php artisan db:seed"** for generate user and role
6. Login to system, enjoy

**Install Package for Automatic Generate Image and Short Video**
1. Install **ffmpeg** and **ffprobe** from your OS Repository
2. Update configuration .env at line **FFMPEG_BIN** and **FFPROB_BIN**
3. Enjoy