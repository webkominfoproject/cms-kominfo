<?php

use Illuminate\Database\Seeder;

class ContentStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content_status')->insert([
            'name' => 'Fokus'
        ]);
        DB::table('content_status')->insert([
            'name' => 'Video'
        ]);
        DB::table('content_status')->insert([
            'name' => 'Foto'
        ]);
        DB::table('content_status')->insert([
            'name' => 'Artikel'
        ]);
    }
}
