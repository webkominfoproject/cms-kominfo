<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
            'first_name' => 'Admin IndonesiaBaik',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Super Admin IndonesiaBaik',
            'email' => 'super@admin.com',
            'password' => bcrypt('admin123'),
        ]);
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'superadmin']);
              
        $admin = User::where('email', 'admin@admin.com')->first()->assignRole('admin');
        $superadmin = User::where('email', 'super@admin.com')->first()->assignRole('superadmin');
    }
}
