<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#004ca3',
        'button'    => '#004cad',

    ],

    'view' => [
        'senderName'  => null,
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,

        'logo'        => [
            'path'   => 'http://cms-kominfo-new.test/image/gpr-logo.jpg',
            'width'  => '123',
            'height' => '123',
        ],

        'twitter'  => null,
        'facebook' => null,
        'flickr'   => null,
    ],

];
