function convertToSlug(Text) {
	return Text
		.toLowerCase()
		.replace(/ /g, '-')
		.replace(/[^\w-]+/g, '');
}

function reInitializationDatePicker(form){
	form.find("#production-date").datepicker("remove");
	form.find('#production-date').datepicker({
		format: 'dd-mm-yyyy',
		todayHighlight: true,
	}).datepicker('setDate', new Date());
}

function setValueDatePicker(form, date){
	form.find("#production-date").datepicker("remove");
	form.find('#production-date').datepicker({
		format: 'dd-mm-yyyy',
		todayHighlight: true,
	}).datepicker("update", Date.parse(date).toString('dd-MM-yyyy'));
}

var app = {
    init : function(){
    	// method call in every page
        handleTooltip._tooltip();
        handleEditor.editor();
        handleClock.digital();
	},

	handleDashboard : function(){
		dashboard.handleDonut();
	},

	handleProfil : function(){
		profil.handleUpdate();
		profil.handleUpdatePic();
	},
	
	handleUserPage : function(){
		user.handleTable();
		user.handleModalShow();
		user.handleModalClose();
		user.handlePostUser();
		user.handleEditUser();
		user.handleDeleteUser();
	},

	handleUserMemberPage : function(){
		usermember.handleTable();
		usermember.handleModalShow();
		usermember.handleModalClose();
		usermember.handlePostUser();
		usermember.handleEditUser();
		usermember.handleDeleteUser();
	},
	
    handleRolePage : function(){
		role.handleTable();
		role.handleModalShow();
		role.handleModalClose();
		role.handlePostData();
		role.handleEditData();
		role.handleDeleteData();
	},

	handleTagsPage : function(){
		tags.handleTable();
		tags.handleModalShow();
		tags.handleModalClose();
		tags.handlePostData();
		tags.handleEditData();
		tags.handleDeleteData();
	},

	handleInstansiPage : function(){
		instansi.handleTable();
		instansi.handleModalShow();
		instansi.handleModalClose();
		instansi.handlePostData();
		instansi.handleEditData();
		instansi.handleDeleteData();
	},

	handleTopicsPage : function(){
		topics.handleTable();
		topics.handleModalShow();
		topics.handleModalClose();
		topics.handlePostData();
		topics.handleEditData();
		topics.handleDeleteData();
	},

	handleVideoPage : function(){
		video.handleTable();
		video.handleModalShow();
		video.handleEmbedShow();
		video.handleModalClose();
		video.handlePostData();
		video.handleContentData();
		video.handleEditData();
		video.handleDeleteData();
	},

	handlePhotoPage : function(){
		photo.handleTable();
		photo.handleModalShow();
		photo.handleModalClose();
		photo.handlePostData();
		photo.handleContentData();
		photo.handleEditData();
		photo.handleDeleteData();
	},

	handleArtikelPage : function(){
		artikel.handleTable();
		artikel.handleModalShow();
		artikel.handleModalClose();
		artikel.handlePostData();
		artikel.handleContentData();
		artikel.handleEditData();
		artikel.handleDeleteData();
	},

	handleBlogPage : function(){
		blog.handleTable();
		blog.handleModalShow();
		blog.handleModalClose();
		blog.handlePostData();
		blog.handleContentData();
		blog.handleEditData();
		blog.handleDeleteData();
	},

	handleNewsPage : function(){
		news.handleTable();
		news.handleModalShow();
		news.handleModalClose();
		news.handlePostData();
		news.handleContentData();
		news.handleEditData();
		news.handleDeleteData();
	},

	handleCategoryPage : function(){
		category.handleTable();
		category.handleModalShow();
		category.handleModalClose();
		category.handlePostData();
		category.handleEditData();
		category.handleDeleteData();
	},

	handleCategoryFotoPage : function(){
		categoryfoto.handleTable();
		categoryfoto.handleModalShow();
		categoryfoto.handleModalClose();
		categoryfoto.handlePostData();
		categoryfoto.handleEditData();
		categoryfoto.handleDeleteData();
	},

	handleCategoryArtikelPage : function(){
		categoryartikel.handleTable();
		categoryartikel.handleModalShow();
		categoryartikel.handleModalClose();
		categoryartikel.handlePostData();
		categoryartikel.handleEditData();
		categoryartikel.handleDeleteData();
	},
};

var dashboard = {
	handleDonut : function(){
		urldonut = baseURL+"/dashboard/data/";
		$.ajax({
			url: urldonut,
			type: "GET",
			dataType: "JSON",
			success : function(data){
				var $donutData = [
		            {label: "Konten Terunduh", value: 0},
		            {label: "Konten Terbagikan", value: 0},
		            {label: "Konten Terlihat", value: 0}
		        ];
		        dashboard.createDonutChart('video-donut', $donutData, ['#f0f1f4', '#10a9d7', '#fab249']);
			}
		});
	},

	createDonutChart : function(element, data, colors){
		 Morris.Donut({
            element: element,
            data: data,
            resize: true,
            colors: colors
        });
	},
};

var profil = {
	handleUpdate : function(){
		$('#form-profil').ajaxForm({
            type: 'POST',
            url: baseURL+'/profil/updateprof',
            beforeSubmit : function(){
                $('#loader').css('display','block');
                $('.progress-bar').css('width','0%').text('0%');
                progressbar.iUploadHandle(true);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var persen = percentComplete + '%';
                $('.progress-bar').css('width',persen).text(persen);
                if(percentComplete == 100){
                    $('.progress-bar').css('width',persen).text('processing');
                }
            },
            complete : function(xhr){
                $('#loader').css('display','none');
                var data = $.parseJSON(xhr.responseText);
                if (data.status == 2) {
                    notification._toast('Terjadi kesalahan', data.message, 'error');
                }else{
                    notification._toast('Update Data Berhasil', data.message, 'success');
                }
                progressbar.iUploadHandle(false);
            }
        });
	},
	handleUpdatePic : function(){
		$('#form-profilpic').ajaxForm({
            type: 'POST',
            url: baseURL+'/profil/updatepic',
            beforeSubmit : function(){
                foto = $('#foto')[0].files[0].size;
                if(foto > 100000000){
                    notification._toast("ERROR", "File too large", "error");
                    return false;
                }
                $('#loader').css('display','block');
                $('.progress-bar').css('width','0%').text('0%');
                progressbar.iUploadHandle(true);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var persen = percentComplete + '%';
                $('.progress-bar').css('width',persen).text(persen);
                if(percentComplete == 100){
                    $('.progress-bar').css('width',persen).text('processing');
                }
            },
            complete : function(xhr){
                $('#loader').css('display','none');
                var data = $.parseJSON(xhr.responseText);
                if (data.status == 2) {
                    notification._toast('Terjadi kesalahan', data.message, 'error');
                }else{
                    notification._toast('Update Data Berhasil', data.message, 'success');
                }
                $("#dataModal").modal("hide");
            }
        });
	},
};

var user = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/user/data',
			ajax: {
                url: baseURL+"/user/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'first_name', name: 'first_name' },
				{ data: 'last_name', name: 'last_name' },
				{	data : null,
					name : 'instansi_name',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var instansi = "-";

						if(data.instansi != null){
							instansi = data.instansi.name;
						}

						return instansi;
					}
			 	},
				{ data: 'email', name: 'email', orderable: false }, 
				{	data : null,
					name : 'role',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var datarole = "-";
						if(data.roles.length != 0){
							datarole = data.roles[0]['name'];
						}else{
							datarole = 'Member';
						} 
						var up = datarole;

						return up;
					}
				 },
				 {	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.status == 0){
							status = '<b class="text-danger">Tidak Terverifikasi</b>';
						}else{
							status = '<b class="text-success">Terverifikasi</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						if (data.roles[0].name == "superadmin"){
							var levelUserData = 1;
						}else{
							var levelUserData = 2;
						}
						var button = "<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle m-r-5 btn-edt-user dotip' data-toggle='tooltip' title='Edit User'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/user/destroy/"+data.id+"' class='btn btn-warning btn-outline btn-circle m-r-5 btn-delete-user dotip' data-toggle='tooltip' title='Delete User'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/user/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline User'>"
									+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/user/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve User'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.status == 0){
							return button+approve;
						}else if(levelUser == 1 && data.status == 1){
							return button+decline;
						}else if (levelUser == 2){
							if (levelUserData != 1){
								return button;
							}
						}
					}
				}
			],
		});
		user.handleInfoUser();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Tambah Pengguna");
			form.find("#method").val("store");
			form.find(".password_form").css("display", "flex");
		})
	},

	handlePostUser : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				// everything looks good!
				if($(this).find("#password").val() != $(this).find("#confirmation").val()){
					$("#password").parent().addClass("has-error");
					$("#confirmation").parent().addClass("has-error");
					notification._toast('ERROR', 'Password tidak sama', 'error');		
				} else {
					var data = $(this).serialize();
					if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
						var url = baseURL+"/user/store";
					} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
						var url = baseURL+"/user/update/"+$(this).find("#id").val();
					}
					user.hanldeStoreData(url, data);
				}
				return false;
			} else {
				notification._toast('ERROR', 'Tolong mengisi data', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Update Data Berhasil', data.message, 'success');
					$("#dataModal").modal("hide");
					user.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditUser : function(){
		$("#dataTable tbody").on("click", ".btn-edt-user",function(){
			$.ajax({
				url: baseURL+"/user/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					user.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		if(data.roles.length != 0){
			datarole = data.roles[0]['name'];
		}else{
			datarole = 'Member';
		} 

		modal.modal("show");
		modal.find(".modal-title").text("Update User");
		
		form.find("#id").val(data.id);
		form.find("#first_name").val(data.first_name);
		form.find("#last_name").val(data.last_name);
		form.find("#nik").val(data.nik);
		form.find("#jabatan").val(data.jabatan);
		form.find("#instansi_id").select2().select2('val', [data.instansi_id]);
		form.find("#email").val(data.email);
		form.find("#role").val(datarole);
		form.find(".password_form").css("display", "none");
		form.find("#method").val("update");
	},

	handleDeleteUser : function(){
		$("#dataTable tbody").on("click", ".btn-delete-user", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', data.message, 'success');
					user.handleTable();
				}
			});
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					user.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					user.handleTable();
				}
			});
		});
	},

	handleInfoUser : function(){
		$.ajax({
			url: baseURL+"/user/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
				$('#total').html(data.total);
			}
		});
	}
};

var usermember = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/user/data',
			ajax: {
                url: baseURL+"/usermember/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'first_name', name: 'first_name' },
				{ data: 'last_name', name: 'last_name' },
				{	data : null,
					name : 'instansi_name',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var instansi = "-";

						if(data.instansi != null){
							instansi = data.instansi.name;
						}

						return instansi;
					}
			 	},
				{ data: 'email', name: 'email', orderable: false }, 
				{	data : null,
					name : 'role',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var datarole = "-";
						if(data.roles.length != 0){
							datarole = data.roles[0]['name'];
						}else{
							datarole = 'Member';
						} 
						var up = datarole;

						return up;
					}
				 },
				 {	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.status == 0){
							status = '<b class="text-danger">Tidak Terverifikasi</b>';
						}else{
							status = '<b class="text-success">Terverifikasi</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle m-r-5 btn-edt-user dotip' data-toggle='tooltip' title='Edit User'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/usermember/destroy/"+data.id+"' class='btn btn-warning btn-outline btn-circle m-r-5 btn-delete-user dotip' data-toggle='tooltip' title='Delete User'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/usermember/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline User'>"
									+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/usermember/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve User'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.status == 0){
							return button+approve;
						}else if(levelUser == 1 && data.status == 1){
							return button+decline;
						}else{
							return button;
						}
					}
				}
			],
		});
		usermember.handleInfoUser();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Tambah Pengguna");
			form.find("#method").val("store");
			form.find(".password_form").css("display", "flex");
		})
	},

	handlePostUser : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				// everything looks good!
				if($(this).find("#password").val() != $(this).find("#confirmation").val()){
					$("#password").parent().addClass("has-error");
					$("#confirmation").parent().addClass("has-error");
					notification._toast('ERROR', 'Password tidak sama', 'error');		
				} else {
					var data = $(this).serialize();
					if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
						var url = baseURL+"/usermember/store";
					} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
						var url = baseURL+"/usermember/update/"+$(this).find("#id").val();
					}
					usermember.hanldeStoreData(url, data);
				}
				return false;
			} else {
				notification._toast('ERROR', 'Tolong mengisi data', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Update Data Berhasil', data.message, 'success');
					$("#dataModal").modal("hide");
					usermember.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditUser : function(){
		$("#dataTable tbody").on("click", ".btn-edt-user",function(){
			$.ajax({
				url: baseURL+"/usermember/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					usermember.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		if(data.roles.length != 0){
			datarole = data.roles[0]['name'];
		}else{
			datarole = 'Member';
		} 

		modal.modal("show");
		modal.find(".modal-title").text("Update User");
		
		form.find("#id").val(data.id);
		form.find("#first_name").val(data.first_name);
		form.find("#last_name").val(data.last_name);
		form.find("#nik").val(data.nik);
		form.find("#jabatan").val(data.jabatan);
		form.find("#instansi_id").val(data.instansi_id);
		form.find("#email").val(data.email);
		form.find("#role").val(datarole);
		form.find(".password_form").css("display", "none");
		form.find("#method").val("update");
	},

	handleDeleteUser : function(){
		$("#dataTable tbody").on("click", ".btn-delete-user", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', data.message, 'success');
					usermember.handleTable();
				}
			});
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					usermember.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					usermember.handleTable();
				}
			});
		});
	},

	handleInfoUser : function(){
		$.ajax({
			url: baseURL+"/usermember/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
				$('#total').html(data.total);
			}
		});
	}
};

var role = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/role/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Role'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/role/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Role'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						return button;

					}
				}
			],
		});
		role.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Create User Role");
			form.find("#method").val("store");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				// everything looks good!
				if($(this).find("#password").val() != $(this).find("#confirmation").val()){
					$("#password").parent().addClass("has-error");
					$("#confirmation").parent().addClass("has-error");
					notification._toast('ERROR', 'Password did\'nt match', 'error');		
				} else {
					var data = $(this).serialize();
					if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
						var url = baseURL+"/role/store";
					} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
						var url = baseURL+"/role/update/"+$(this).find("#id").val();
					}
					role.hanldeStoreData(url, data);
				}
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					role.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/role/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					role.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Role");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					role.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/role/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};

var tags = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/tags/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Tags'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/tags/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Tags'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						return button;
					}
				}
			],
		});
		tags.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Create Tags");
			form.find("#method").val("store");
			form.find("#id").val("");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				var data = $(this).serialize();
				if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
					var url = baseURL+"/tags/store";
				} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
					var url = baseURL+"/tags/update/"+$(this).find("#id").val();
				}
				tags.hanldeStoreData(url, data);
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					tags.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/tags/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					tags.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Tags");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					tags.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/tags/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};

var instansi = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/instansi/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{ data: 'address', name: 'address' },
				{ data: 'email', name: 'email' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Instansi'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/instansi/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Instansi'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						return button;
					}
				}
			],
		});
		instansi.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Create Instansi");
			form.find("#method").val("store");
			form.find("#id").val("");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				var data = $(this).serialize();
				if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
					var url = baseURL+"/instansi/store";
				} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
					var url = baseURL+"/instansi/update/"+$(this).find("#id").val();
				}
				instansi.hanldeStoreData(url, data);
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					instansi.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/instansi/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					instansi.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Instansi");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#address").val(data.address);
		form.find("#email").val(data.email);
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					instansi.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/instansi/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};

var topics = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/topics/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Topics'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/topics/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Topics'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						return button;
					}
				}
			],
		});
		topics.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Create Topics");
			form.find("#method").val("store");
			form.find("#id").val("");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				var data = $(this).serialize();
				if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
					var url = baseURL+"/topics/store";
				} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
					var url = baseURL+"/topics/update/"+$(this).find("#id").val();
				}
				topics.hanldeStoreData(url, data);
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					topics.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/topics/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					topics.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Topics");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					topics.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/topics/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};

var video = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/video/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{ data: 'topics.name', name: 'topics.name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){

						user = '-';
						if (data.user != null) {
							user = data.user.first_name+' '+data.user.last_name;
						}

						return user;
					}
				},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-content-data' data-toggle='tooltip' title='Content'>"
										+"<i class='ti-file'></i>"
									+"</button>";
						return button;
					}
				},
				{	data : null,
					name : 'verified',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.verified == 0){
							status = '<b class="text-danger">Tidak Terverifikasi</b>';
						}else{
							status = '<b class="text-success">Terverifikasi</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-info btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Video'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/video/destroy/"+data.id+"' class='btn dotip btn-warning btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Video'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/video/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline Video'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/video/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve Video'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.verified == 0){
							return button+approve;
						}else if(levelUser == 1 && data.verified == 1){
							return button+decline;
						}else{
							return button;
						}
					}
				}
			],
		});
		video.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");
			reInitializationDatePicker(form);

			modal.modal("show");
			modal.find(".modal-title").text("Create Video");
			modal.find('#content-form').css('display','flex');
			modal.find('#content-file').css('display','flex');
			form.find('#thumbnailimage').attr('src', baseURL+'/image/placeholder-image.png');
			tinymce.get('description').setContent('');
			form.find('#prevshortvideo')[0].src = '';
			form.find('#prevshortvideo').parent()[0].load();
			form.find('#prevfullvideo')[0].src = '';
			form.find('#prevfullvideo').parent()[0].load();
			form.find('#thumbnail').prop('required',true);
			form.find('#shortvideo').prop('required',true);
			form.find('#fullvideo').prop('required',true);
			form.find('#production-date').prop('required',true);
			form.find('#name').prop('required',true);
			form.find('#slug').prop('required',true);
			form.find('#description').attr('required',true);
			form.find('#topics_id').attr('required',true);
			form.find("#method").val("store");

			$("#content-file-simple").css('display','flex');
			$("#simple-mode").prop('checked', false);
			$('#content-file-simple-video').css('display','none');

			$('#content-file-youtube').css('display','none');
			$('#urlyoutube').prop('required',false);
		});

		$("#name").keyup(function(){
			var Text = $(this).val();
			Text = convertToSlug(Text);
			$("#slug").val(Text);    
		});

		
		$( "#simple-mode" ).on( "click", function() {
			var modal = $("#dataModal");
			var form = $("#dataForm");

			if ($(this).prop( "checked" )){
				$('#content-file-simple-video').css('display','flex');
				$('#fullvideosimple').prop('required',true);
				
				$('#content-file').css('display','none');
				$('#thumbnail').prop('required',false);
				$('#shortvideo').prop('required',false);
				$('#fullvideo').prop('required',false);

				$('#content-file-youtube').css('display','none');
				$('#urlyoutube').prop('required',false);
			}else{
				$('#content-file-simple-video').css('display','none');
				$('#fullvideosimple').prop('required',false);
				
				$('#content-file').css('display','flex');
				$('#thumbnail').prop('required',true);
				$('#shortvideo').prop('required',true);
				$('#fullvideo').prop('required',true);

				$('#content-file-youtube').css('display','none');
				$('#urlyoutube').prop('required',false);
			}

		});
	},

	handleEmbedShow: function(){
		$("#add-youtube").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Embed URL Youtube");
			modal.find('#content-form').css('display','flex');
			modal.find('#content-file').css('display','flex');
			form.find('#name').prop('required',true);
			form.find('#slug').prop('required',true);
			form.find('#description').attr('required',true);
			form.find('#topics_id').attr('required',true);
			form.find("#method").val("store");

			$("#content-file-simple").css('display','none');
			$("#simple-mode").prop('checked', true);
			$('#content-file-simple-video').css('display','none');
			$('#fullvideosimple').prop('required',false);
			$('#content-file').css('display','none');
			$('#thumbnail').prop('required',false);
			$('#shortvideo').prop('required',false);
			$('#fullvideo').prop('required',false);

			$('#content-file-youtube').css('display','flex');
			$('#urlyoutube').prop('required',true);

		});

		$("#name").keyup(function(){
			var Text = $(this).val();
			Text = convertToSlug(Text);
			$("#slug").val(Text);    
		});
	},

	handlePostData : function(){
		$('#dataForm').ajaxForm({
			type: 'POST',
			url: baseURL+'/video/store',
			beforeSubmit : function(){
				if ($('#method') == 'store'){
					thumbnail = $('#thumbnail')[0].files[0].size;
					shortvideo = $('#shortvideo')[0].files[0].size;
					fullvideo = $('#fullvideo')[0].files[0].size;
					if(thumbnail > 100000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
					if(shortvideo > 200000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
					if(fullvideo > 200000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
				}
				$('#loader').css('display','block');
				$('.progress-bar').css('width','0%').text('0%');
				progressbar.iUploadHandle(true);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var persen = percentComplete + '%';
				$('.progress-bar').css('width',persen).text(persen);
				if(percentComplete == 100){
					$('.progress-bar').css('width',persen).text('processing');
				}
			},
			complete : function(xhr){
				$('#loader').css('display','none');
				var data = $.parseJSON(xhr.responseText);

				if (data.status == 2) {
					notification._toast('Terjadi kesalahan', data.message, 'error');
				}else{
					notification._toast('Update Data Berhasil', data.message, 'success');
				}
				$("#dataModal").modal("hide");
				video.handleTable();
				progressbar.iUploadHandle(false);
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('#loader').css('display','none');
			},
			success: function(data) {
				$('#loader').css('display','none');
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					video.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea, input[type=file]").val("");
			$(this).find('#tags').val(null).trigger('change');
		})
	},

	handleContentData : function(){
		$("#dataTable tbody").on("click", ".btn-content-data",function(){
			$.ajax({
				url: baseURL+"/video/content/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					video.handleShowContentForm(data);
				}
			});
		})
	},

	handleShowContentForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		modal.modal("show");
		modal.find(".modal-title").text("Content Video");
		modal.find('#content-form').css('display','none');
		modal.find('#content-file').css('display','flex');
		form.find('#thumbnailimage').attr('src', mediaURL+data.thumbnail);
		form.find('#prevshortvideo')[0].src = mediaURL+data.shortvideo;
		form.find('#prevshortvideo').parent()[0].load();
		form.find('#prevfullvideo')[0].src = mediaURL+data.fullvideo;
		form.find('#prevfullvideo').parent()[0].load();
		form.find('#thumbnail').attr('required',false);
		form.find('#shortvideo').attr('required',false);
		form.find('#fullvideo').attr('required',false);
		form.find('#instansi_id').attr('required',false);
		form.find('#name').attr('required',false);
		form.find('#slug').attr('required',false);
		form.find('#tags').attr('required',false);
		form.find('#description').attr('required',false);
		form.find('#topics_id').attr('required',false);
		form.find('#production-date').attr('required',false);
		form.find("#id").val(data.id);
		form.find("#method").val("content");

		$("#content-file-simple").css('display','flex');
		$("#simple-mode").prop('checked', false);
		$('#content-file-simple-video').css('display','none');
		$('#fullvideosimple').attr('required',false);
		
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/video/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					video.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		

		modal.modal("show");
		modal.find(".modal-title").text("Update video");

		modal.find('#content-form').css('display','flex');
		modal.find('#content-file').css('display','none');
		form.find('#thumbnail').prop('required',false);
		form.find('#shortvideo').prop('required',false);
		form.find('#fullvideo').prop('required',false);
		form.find('#name').prop('required',true);
		form.find('#description').prop('required',true);
		form.find("#id").val(data.data.id);
		form.find("#name").val(data.data.name);
		form.find("#slug").val(data.data.slug);
		form.find("#topics_id").val(data.data.topics_id).trigger('change');
		form.find("#instansi_id").val(data.data.instansi_id).trigger('change');
		form.find("#category_id").val(data.data.category_id).trigger('change');
		form.find('#tags').select2('val', [data.tags]);
		form.find("#method").val("update");
		form.find('#description').val(data.data.description);
		tinymce.get('description').setContent(data.data.description);
		setValueDatePicker(form, data.data.content_production);

		$('#content-file-simple').css('display','none');
		$('#content-file-simple-video').css('display','none');
		$('#fullvideosimple').prop('required',false);
		
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				beforeSend: function(){
					$('#loaders').css('display','block');
				},
				success: function(data){
					$('#loaders').css('display','none');
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Content', 'success');
					video.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					video.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					video.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/video/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
			}
		});
	}
};

var photo = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/photo/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{ data: 'topics.name', name: 'topics.name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){

						user = '-';
						if (data.user != null) {
							user = data.user.first_name+' '+data.user.last_name;
						}

						return user;
					}
				},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-content-data' data-toggle='tooltip' title='Content'>"
										+"<i class='ti-file'></i>"
									+"</button>";
						return button;
					}
				},
				{	data : null,
					name : 'verified',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.verified == 0){
							status = '<b class="text-danger">Belum Terverifikasi</b>';
						}else{
							status = '<b class="text-success">Terverifikasi</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-info btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Photo'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/photo/destroy/"+data.id+"' class='btn dotip btn-warning btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Photo'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/photo/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline Photo'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/photo/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve Photo'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.verified == 0){
							return button+approve;
						}else if(levelUser == 1 && data.verified == 1){
							return button+decline;
						}else{
							return button;
						}
					}
				}
			],
		});
		photo.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			reInitializationDatePicker(form);

			modal.modal("show");
			modal.find(".modal-title").text("Create Photo");
			modal.find('#content-form').css('display','flex');
			modal.find('#content-file').css('display','flex');
			form.find('#fotoimage').attr('src', baseURL+'/image/placeholder-image.png');
			tinymce.get('description').setContent('');
			form.find('#foto').prop('required',true);
			form.find('#name').prop('required',true);
			form.find('#slug').prop('required',true);
			form.find('#description').attr('required',true);
			form.find('#topics_id').attr('required',true);
			form.find("#method").val("store");
		});

		$("#name").keyup(function(){
			var Text = $(this).val();
			Text = convertToSlug(Text);
			$("#slug").val(Text);    
		});
	},

	handlePostData : function(){
		$('#dataForm').ajaxForm({
			type: 'POST',
			url: baseURL+'/photo/store',
			beforeSubmit : function(){
				if ($('#method') == 'store'){
					foto = $('#foto')[0].files[0].size;
					if(foto > 100000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
				}
				$('#loader').css('display','block');
				$('.progress-bar').css('width','0%').text('0%');
				progressbar.iUploadHandle(true);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var persen = percentComplete + '%';
				$('.progress-bar').css('width',persen).text(persen);
				if(percentComplete == 100){
					$('.progress-bar').css('width',persen).text('processing');
				}
			},
			complete : function(xhr){
				$('#loader').css('display','none');
				var data = $.parseJSON(xhr.responseText);
				if (data.status == 2) {
					notification._toast('Terjadi kesalahan', data.message, 'error');
				}else{
					notification._toast('Update Data Berhasil', data.message, 'success');
				}
				$("#dataModal").modal("hide");
				photo.handleTable();
				progressbar.iUploadHandle(false);
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('#loader').css('display','none');
			},
			success: function(data) {
				$('#loader').css('display','none');
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					photo.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea, input[type=file]").val("");
			$(this).find('#tags').val(null).trigger('change');
		})
	},

	handleContentData : function(){
		$("#dataTable tbody").on("click", ".btn-content-data",function(){
			$.ajax({
				url: baseURL+"/photo/content/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					photo.handleShowContentForm(data);
				}
			});
		})
	},

	handleShowContentForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		modal.modal("show");
		modal.find(".modal-title").text("Content Photo");
		modal.find('#content-form').css('display','none');
		modal.find('#content-file').css('display','flex');
		form.find('#fotoimage').attr('src', mediaURL+data.foto);
		form.find('#foto').attr('required',false);
		form.find('#name').attr('required',false);
		form.find('#description').attr('required',false);
		form.find('#topics_id').attr('required',false);
		form.find("#id").val(data.id);
		form.find("#method").val("content");
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/photo/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					photo.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		modal.modal("show");
		modal.find(".modal-title").text("Update Photo");
		modal.find('#content-form').css('display','flex');
		modal.find('#content-file').css('display','none');
		form.find('#foto').prop('required',false);
		form.find('#name').prop('required',true);
		form.find('#description').prop('required',true);
		form.find('#topics_id').prop('required',true);
		form.find("#id").val(data.data.id);
		form.find("#name").val(data.data.name);
		form.find("#topics_id").val(data.data.topics_id).trigger('change');
		form.find("#instansi_id").val(data.data.instansi_id).trigger('change');
		form.find("#category_id").val(data.data.category_id).trigger('change');
		form.find("#slug").val(data.data.slug);
		form.find('#tags').select2('val', [data.tags]);
		form.find("#method").val("update");
		form.find('#description').val(data.data.description);
		tinymce.get('description').setContent(data.data.description);
		setValueDatePicker(form, data.data.content_production);
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				beforeSend: function(){
					$('#loaders').css('display','block');
				},
				success: function(data){
					$('#loaders').css('display','none');
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Content', 'success');
					photo.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					photo.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					photo.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/photo/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
			}
		});
	}
};

var artikel = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/artikel/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{ data: 'topics.name', name: 'topics.name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						user = '-';
						if (data.user != null) {
							user = data.user.first_name+' '+data.user.last_name;
						}

						return user;
					}
				},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<a href='"+mediaURL+data.contentdetail[0].source+"' target='_blank'><button type='button' class='btn dotip btn-success btn-outline btn-circle m-r-5' data-toggle='tooltip' title='Content'>"
										+"<i class='ti-file'></i>"
									+"</button></a>";
						return button;
					}
				},
				{	data : null,
					name : 'verified',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.verified == 0){
							status = '<b class="text-danger">Belum Terverifikasi</b>';
						}else{
							status = '<b class="text-success">Terverifikasi</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-info btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Artikel'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/artikel/destroy/"+data.id+"' class='btn dotip btn-warning btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Artikel'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/artikel/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline Artikel'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/artikel/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve Artikel'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.verified == 0){
							return button+approve;
						}else if(levelUser == 1 && data.verified == 1){
							return button+decline;
						}else{
							return button;
						}
					}
				}
			],
		});
		artikel.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");
			reInitializationDatePicker(form)
			modal.modal("show");
			modal.find(".modal-title").text("Create Artikel");
			tinymce.get('description').setContent('');
			modal.find('#content-form').css('display','flex');
			modal.find('#content-file').css('display','flex');
			form.find('#artikel').prop('required',true);
			form.find('#name').prop('required',true);
			form.find('#description').attr('required',true);
			form.find('#slug').attr('required',true);
			form.find('#topics_id').attr('required',true);
			form.find("#method").val("store");
		});

		
		$("#name").keyup(function(){
			var Text = $(this).val();
			Text = convertToSlug(Text);
			$("#slug").val(Text);    
		});

	},

	handlePostData : function(){
		$('#dataForm').ajaxForm({
			type: 'POST',
			url: baseURL+'/artikel/store',
			beforeSubmit : function(){
				if ($('#method') == 'store'){
					artikel = $('#artikel')[0].files[0].size;
					if(artikel > 100000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
				}
				$('#loader').css('display','block');
				$('.progress-bar').css('width','0%').text('0%');
				progressbar.iUploadHandle(true);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var persen = percentComplete + '%';
				$('.progress-bar').css('width',persen).text(persen);
				if(percentComplete == 100){
					$('.progress-bar').css('width',persen).text('processing');
				}
			},
			complete : function(xhr){
				$('#loader').css('display','none');
				var data = $.parseJSON(xhr.responseText);
				if (data.status == 2) {
					notification._toast('Terjadi kesalahan', data.message, 'error');
				}else{
					notification._toast('Update Data Berhasil', data.message, 'success');
				}
				$("#dataModal").modal("hide");
				artikel.handleTable();
				progressbar.iUploadHandle(false);
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('#loader').css('display','none');
			},
			success: function(data) {
				$('#loader').css('display','none');
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					artikel.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea, input[type=file]").val("");
			$(this).find('#tags').val(null).trigger('change');
		})
	},

	handleContentData : function(){
		$("#dataTable tbody").on("click", ".btn-content-data",function(){
			$.ajax({
				url: baseURL+"/artikel/content/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					artikel.handleShowContentForm(data);
				}
			});
		})
	},

	handleShowContentForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		modal.modal("show");
		modal.find(".modal-title").text("Content Artikel");
		modal.find('#content-form').css('display','none');
		modal.find('#content-file').css('display','flex');
		form.find('#artikelfile').attr('src', mediaURL+data.artikel);
		form.find('#artikel').attr('required',false);
		form.find('#name').attr('required',false);
		form.find('#description').attr('required',false);
		form.find('#topics_id').attr('required',false);
		form.find("#id").val(data.id);
		form.find("#method").val("content");
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/artikel/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					artikel.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		modal.modal("show");
		modal.find(".modal-title").text("Update Artikel");
		modal.find('#content-form').css('display','flex');
		modal.find('#content-file').css('display','none');
		form.find('#artikel').prop('required',false);
		form.find('#name').prop('required',true);
		form.find('#description').prop('required',true);
		form.find('#topics_id').prop('required',true);
		form.find("#id").val(data.data.id);
		form.find("#name").val(data.data.name);
		form.find("#slug").val(data.data.slug);
		form.find("#topics_id").val(data.data.topics_id).trigger('change');
		form.find("#instansi_id").val(data.data.instansi_id).trigger('change');
		form.find("#category_id").val(data.data.category_id).trigger('change');
		setValueDatePicker(form, data.data.content_production)
		form.find('#tags').select2('val', [data.tags]);
		form.find("#method").val("update");
		form.find('#description').val(data.data.description);
		tinymce.get('description').setContent(data.data.description);
		
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				beforeSend: function(){
					$('#loaders').css('display','block');
				},
				success: function(data){
					$('#loaders').css('display','none');
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Content', 'success');
					artikel.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					artikel.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					artikel.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/artikel/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
			}
		});
	}
};

var blog = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/fokus/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'title', name: 'title' },
				{ data: 'topics.name', name: 'topics.name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						user = '-';
						if (data.user != null) {
							user = data.user.first_name+' '+data.user.last_name;
						}

						return user;
					}
				},
				{	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.verified == 0){
							status = '<b class="text-danger">Tidak terverifikasi</b>';
						}else{
							status = '<b class="text-success">Terverifikasi</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-info btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Artikel'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/fokus/destroy/"+data.id+"' class='btn dotip btn-warning btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Artikel'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/fokus/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline Artikel'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/fokus/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve Artikel'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.verified == 0){
							return button+approve;
						}else if(levelUser == 1 && data.verified == 1){
							return button+decline;
						}else{
							return button;
						}
					}
				}
			],
		});
		blog.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");
			reInitializationDatePicker(form)
			modal.modal("show");
			modal.find(".modal-title").text("Create Fokus");
			tinymce.get('description').setContent('');
			modal.find('#content-form').css('display','flex');
			form.find('#fotoimage').attr('src', baseURL+'/image/placeholder-image.png');
			form.find('#title').prop('required',true);
			form.find('#description').attr('required',true);
			form.find('#desc').attr('required',true);
			form.find('#foto').attr('required',true);
			form.find('#topics_id').attr('required',true);
			form.find("#method").val("store");

		});

		$("#title").keyup(function(){
			var Text = $(this).val();
			Text = convertToSlug(Text);
			$("#slug").val(Text);    
		});
	},

	handlePostData : function(){
		$('#dataForm').ajaxForm({
			type: 'POST',
			url: baseURL+'/fokus/store',
			beforeSubmit : function(){
				if ($('#method') == 'store'){
					foto = $('#foto')[0].files[0].size;
					shortvideo = $('#shortvideo')[0].files[0].size;
					if(foto > 100000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
					if(shortvideo > 100000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
				}
				$('#loader').css('display','block');
				$('.progress-bar').css('width','0%').text('0%');
				progressbar.iUploadHandle(true);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var persen = percentComplete + '%';
				$('.progress-bar').css('width',persen).text(persen);
				if(percentComplete == 100){
					$('.progress-bar').css('width',persen).text('processing');
				}
			},
			complete : function(xhr){
				$('#loader').css('display','none');
				var data = $.parseJSON(xhr.responseText);
				if (data.status == 2) {
					notification._toast('Terjadi kesalahan', data.message, 'error');
				}else{
					notification._toast('Update Data Berhasil', data.message, 'success');
				}
				$("#dataModal").modal("hide");
				blog.handleTable();
				progressbar.iUploadHandle(false);
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('#loader').css('display','none');
			},
			success: function(data) {
				$('#loader').css('display','none');
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					artikel.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea, input[type=file]").val("");
			$(this).find('#tags').val(null).trigger('change');
		})
	},

	handleContentData : function(){
		$("#dataTable tbody").on("click", ".btn-content-data",function(){
			$.ajax({
				url: baseURL+"/fokus/content/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					artikel.handleShowContentForm(data);
				}
			});
		})
	},

	handleShowContentForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		modal.modal("show");
		
		modal.find(".modal-title").text("Content Artikel");
		modal.find('#content-form').css('display','none');
		modal.find('#content-file').css('display','flex');
		form.find('#artikelfile').attr('src', mediaURL+data.artikel);
		form.find('#artikel').attr('required',false);
		form.find('#title').attr('required',false);
		form.find('#description').attr('required',false);
		form.find('#topics_id').attr('required',false);
		form.find("#id").val(data.id);
		form.find("#method").val("content");
		
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/fokus/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					blog.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Fokus");
		modal.find('#content-form').css('display','flex');
		form.find('#title').prop('required',true);
		form.find('#description').prop('required',true);
		form.find('#desc').prop('required',true);
		form.find('#foto').prop('required',false);
		form.find('#topics_id').prop('required',true);
		form.find("#id").val(data.id);
		form.find("#title").val(data.title);
		form.find("#desc").val(data.description);
		form.find('#fotoimage').attr('src', mediaURL+data.image);
		form.find("#topics_id").val(data.topics_id).trigger('change');
		form.find("#instansi_id").val(data.instansi_id).trigger('change');
		setValueDatePicker(form, data.content_production)
		form.find('#description').val(data.content);
		form.find('#slug').val(data.slug);
		form.find("#method").val("update");
		
		
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				beforeSend: function(){
					$('#loaders').css('display','block');
				},
				success: function(data){
					$('#loaders').css('display','none');
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Content', 'success');
					blog.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					blog.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					blog.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/fokus/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
			}
		});
	}
};

var news = {
	handleTable : function(){
		$('#dataTable').DataTable({
			scrollX: true,
			stateSave: true,
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/news/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'title', name: 'title' },
				{ data: 'topics.name', name: 'topics.name' },
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						user = '-';
						if (data.user != null) {
							user = data.user.first_name+' '+data.user.last_name;
						}

						return user;
					}
				},
				{	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.verified == 0){
							status = '<b class="text-danger">UNVERIFIED</b>';
						}else{
							status = '<b class="text-success">VERIFIED</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-info btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Edit Artikel'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/news/destroy/"+data.id+"' class='btn dotip btn-warning btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Delete Artikel'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/news/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Decline Artikel'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/news/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Approve Artikel'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.verified == 0){
							return button+approve;
						}else if(levelUser == 1 && data.verified == 1){
							return button+decline;
						}else{
							return button;
						}
					}
				}
			],
		});
		news.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Create News");
			tinymce.get('description').setContent('');
			modal.find('#content-form').css('display','flex');
			form.find('#fotoimage').attr('src', baseURL+'/image/placeholder-image.png');
			form.find('#title').prop('required',true);
			form.find('#description').attr('required',true);
			form.find('#desc').attr('required',true);
			form.find('#foto').attr('required',true);
			form.find('#topics_id').attr('required',true);
			form.find("#method").val("store");
		});

		$("#title").keyup(function(){
			var Text = $(this).val();
			Text = convertToSlug(Text);
			$("#slug").val(Text);    
		});
	},

	handlePostData : function(){
		$('#dataForm').ajaxForm({
			type: 'POST',
			url: baseURL+'/news/store',
			beforeSubmit : function(){
				if ($('#method') == 'store'){
					foto = $('#foto')[0].files[0].size;
					if(foto > 100000000){
						notification._toast("ERROR", "File too large", "error");
						return false;
					}
				}
				$('#loader').css('display','block');
				$('.progress-bar').css('width','0%').text('0%');
				progressbar.iUploadHandle(true);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var persen = percentComplete + '%';
				$('.progress-bar').css('width',persen).text(persen);
				if(percentComplete == 100){
					$('.progress-bar').css('width',persen).text('processing');
				}
			},
			complete : function(xhr){
				$('#loader').css('display','none');
				var data = $.parseJSON(xhr.responseText);
				if (data.status == 2) {
					notification._toast('Terjadi kesalahan', data.message, 'error');
				}else{
					notification._toast('Update Data Berhasil', data.message, 'success');
				}
				$("#dataModal").modal("hide");
				news.handleTable();
				progressbar.iUploadHandle(false);
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('#loader').css('display','none');
			},
			success: function(data) {
				$('#loader').css('display','none');
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					artikel.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea, input[type=file]").val("");
			$(this).find('#tags').val(null).trigger('change');
		})
	},

	handleContentData : function(){
		$("#dataTable tbody").on("click", ".btn-content-data",function(){
			$.ajax({
				url: baseURL+"/news/content/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					artikel.handleShowContentForm(data);
				}
			});
		})
	},

	handleShowContentForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");

		modal.modal("show");
		modal.find(".modal-title").text("Content Artikel");
		modal.find('#content-form').css('display','none');
		modal.find('#content-file').css('display','flex');
		form.find('#artikelfile').attr('src', mediaURL+data.artikel);
		form.find('#artikel').attr('required',false);
		form.find('#title').attr('required',false);
		form.find('#description').attr('required',false);
		form.find('#topics_id').attr('required',false);
		form.find("#id").val(data.id);
		form.find("#method").val("content");
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/news/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					news.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		

		modal.modal("show");
		modal.find(".modal-title").text("Update News");
		modal.find('#content-form').css('display','flex');
		form.find('#title').prop('required',true);
		form.find('#description').prop('required',true);
		form.find('#desc').prop('required',true);
		form.find('#foto').prop('required',false);
		form.find('#topics_id').prop('required',true);
		form.find("#id").val(data.id);
		form.find("#title").val(data.title);
		form.find("#desc").val(data.description);
		form.find('#fotoimage').attr('src', mediaURL+data.image);
		form.find("#topics_id").val(data.topics_id);
		form.find('#description').val(data.content);
		form.find('#slug').val(data.slug);
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				beforeSend: function(){
					$('#loaders').css('display','block');
				},
				success: function(data){
					$('#loaders').css('display','none');
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Content', 'success');
					news.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					news.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					news.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/news/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
				$('#totalverified').html(data.totalverified);
				$('#totalunverified').html(data.totalunverified);
			}
		});
	}
};

var category = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/category/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.status == "nonaktif"){
							status = '<b class="text-danger">Nonaktif</b>';
						}else{
							status = '<b class="text-success">Aktif</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/category/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Buat Nonaktif'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/category/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Aktifkan'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Rubah'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/category/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Hapus'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.status == "nonaktif"){
							return button+approve;
						}else if(levelUser == 1 && data.status == "aktif"){
							return button+decline;
						}else{
							return button;
						}
						return button;
					}
				}
			],
		});
		category.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Tambah Kategori");
			form.find("#method").val("store");
			form.find("#id").val("");
			form.find("#category-group").val("video");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				var data = $(this).serialize();
				if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
					var url = baseURL+"/category/store";
				} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
					var url = baseURL+"/category/update/"+$(this).find("#id").val();
				}
				category.hanldeStoreData(url, data);
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					category.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/category/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					category.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Topics");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#description-category").val(data.description);
		form.find("#status_kategori").val(data.status).trigger('change');
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					category.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					category.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					category.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/category/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};

var categoryfoto = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/categoryfoto/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.status == "nonaktif"){
							status = '<b class="text-danger">Nonaktif</b>';
						}else{
							status = '<b class="text-success">Aktif</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/category/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Buat Nonaktif'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/category/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Aktifkan'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Rubah'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/category/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Hapus'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.status == "nonaktif"){
							return button+approve;
						}else if(levelUser == 1 && data.status == "aktif"){
							return button+decline;
						}else{
							return button;
						}
						return button;
					}
				}
			],
		});
		categoryfoto.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Tambah Kategori");
			form.find("#method").val("store");
			form.find("#id").val("");
			form.find("#category-group").val("foto");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				var data = $(this).serialize();
				if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
					var url = baseURL+"/categoryfoto/store";
				} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
					var url = baseURL+"/categoryfoto/update/"+$(this).find("#id").val();
				}
				categoryfoto.hanldeStoreData(url, data);
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					categoryfoto.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/categoryfoto/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					categoryfoto.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Topics");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#description-category").val(data.description);
		form.find("#status_kategori").val(data.status).trigger('change');
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					categoryfoto.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					categoryfoto.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					categoryfoto.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/category/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};

var categoryartikel = {
	handleTable : function(){
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			destroy: true,
			// ajax: '/roles/data',
			ajax: {
                url: baseURL+"/categoryartikel/data",
                method: 'GET',
            },
			columns: [
				// { data: 'id', name: 'id', className: "text-center", searchable: false },
				{
					data: null,
					orderable: false,
					className : "text-center",
					searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{ data: 'name', name: 'name' },
				{	data : null,
					name : 'status',
					orderable : true,
					className : "text-center",
					searchable : true,
					render : function(data, type, row) {
						var status = "-";
						if(data.status == "nonaktif"){
							status = '<b class="text-danger">Nonaktif</b>';
						}else{
							status = '<b class="text-success">Aktif</b>';
						} 
						return status;
					}
			 	},
				{
					data: null,
					orderable: false,
					className: "text-center",
					searchable: false,
					render: function(data, type, row){
						var decline = "<a data-toggle='modal' data-target='#declineModal'><button type='button' data-url='"+baseURL+"/category/decline/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-decline-data' data-toggle='tooltip' title='Buat Nonaktif'>"
										+"<i class='ti-close'></i>"
									+"</button></a>";
						var approve ="<a data-toggle='modal' data-target='#approveModal'><button type='button' data-url='"+baseURL+"/category/approve/"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-approve-data' data-toggle='tooltip' title='Aktifkan'>"
										+"<i class='ti-check'></i>"
									+"</button></a>";
						var button = "<button type='button' data-id='"+data.id+"' class='btn dotip btn-success btn-outline btn-circle m-r-5 btn-edt-data' data-toggle='tooltip' title='Rubah'>"
										+"<i class='ti-pencil-alt'></i>"
									+"</button>"
									// +"<button type='button' data-id='"+data.id+"' class='btn btn-info btn-outline btn-circle btn-sm m-r-5 btn-show-permission' data-toggle='tooltip' title='Show Permission'>"
									//	+"<i class='fa fa-certificate'></i>"
									// +"</button>"
									+"<a data-toggle='modal' data-target='#deleteModal'><button type='button' data-url='"+baseURL+"/category/destroy/"+data.id+"' class='btn dotip btn-danger btn-outline btn-circle m-r-5 btn-delete-data' data-toggle='tooltip' title='Hapus'>"
										+"<i class='ti-trash'></i>"
									+"</button></a>";
						if(levelUser == 1 && data.status == "nonaktif"){
							return button+approve;
						}else if(levelUser == 1 && data.status == "aktif"){
							return button+decline;
						}else{
							return button;
						}
						return button;
					}
				}
			],
		});
		categoryartikel.handleInfoData();
	},

	handleModalShow: function(){
		$("#add-data").on("click", function(){
			var modal = $("#dataModal");
			var form = $("#dataForm");

			modal.modal("show");
			modal.find(".modal-title").text("Tambah Kategori");
			form.find("#method").val("store");
			form.find("#id").val("");
			form.find("#category-group").val("artikel");
		})
	},

	handlePostData : function(){
		$('#dataForm').validator(['validate']).on('submit', function (e) {
			if (!e.isDefaultPrevented()) {
				var data = $(this).serialize();
				if($(this).find("#id").val() == "" && $(this).find("#method").val() === "store"){
					var url = baseURL+"/categoryartikel/store";
				} else if($(this).find("#id").val() != "" && $(this).find("#method").val() === "update"){
					var url = baseURL+"/categoryartikel/update/"+$(this).find("#id").val();
				}
				categoryartikel.hanldeStoreData(url, data);
				return false;
			} else {
				notification._toast('ERROR', 'Please input value', 'error');
			}
		});
	},

	hanldeStoreData : function(url, data){
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			data: data,
			success: function(data) {
				if(data.status == 1){
					notification._toast('Success', 'Success Update Data', 'success');
					$("#dataModal").modal("hide");
					categoryartikel.handleTable();
				}else{
					notification._toast('Error', data.message, 'error');
				}
			}, 
		});
	},

	handleModalClose : function(){
		$('#dataModal').on('hidden.bs.modal', function () {
			$(this).find(".has-error").removeClass("has-error");
			$('#dataForm').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		})
	},

	handleEditData : function(){
		$("#dataTable tbody").on("click", ".btn-edt-data",function(){
			$.ajax({
				url: baseURL+"/categoryartikel/edit/"+$(this).attr("data-id"),
				type: "GET",
				dataType: "JSON",
				success : function(data){
					categoryartikel.handleShowEditForm(data);
				}
			});
		})
	},

	handleShowEditForm : function(data){
		var modal = $("#dataModal");
		var form = $("#dataForm");
		
		modal.modal("show");
		modal.find(".modal-title").text("Update Topics");

		form.find("#id").val(data.id);
		form.find("#name").val(data.name);
		form.find("#description-category").val(data.description);
		form.find("#status_kategori").val(data.status).trigger('change');
		form.find("#method").val("update");
	},

	handleDeleteData : function(){
		$("#dataTable tbody").on("click", ".btn-delete-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-approve-data", function(){
			url = $(this).attr('data-url');
		});

		$("#dataTable tbody").on("click", ".btn-decline-data", function(){
			url = $(this).attr('data-url');
		});

		$('#btn-hapus').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#deleteModal').modal('hide');
					notification._toast('Success', 'Success Delete Data', 'success');
					categoryartikel.handleTable();
				}
			});
		});

		$('#btn-approve').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#approveModal').modal('hide');
					notification._toast('Success', 'Success Approve Content', 'success');
					categoryartikel.handleTable();
				}
			});
		});

		$('#btn-decline').on('click',function(){
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'JSON',
				success: function(data){
					$('#declineModal').modal('hide');
					notification._toast('Success', 'Success Decline Content', 'success');
					categoryartikel.handleTable();
				}
			});
		});
	},

	handleInfoData : function(){
		$.ajax({
			url: baseURL+"/categoryartikel/info",
			type: 'GET',
			dataType: 'JSON',
			success: function(data){
				$('#total').html(data.total);
			}
		});
	}
};


function formatRupiah(angka, prefix) {
	var number_string = angka.replace(/[^,\d]/g, "").toString(),
		split = number_string.split(","),
		sisa = split[0].length % 3,
		rupiah = split[0].substr(0, sisa),
		ribuan = split[0].substr(sisa).match(/\d{3}/gi);

	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if (ribuan) {
		separator = sisa ? "." : "";
		rupiah += separator + ribuan.join(".");
	}

	rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
	return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}

var notification = {
	_toast : function(heading, text, icon){
		$.toast({
            heading: heading,
            text: text,
            position: 'top-right',
            loaderBg: '#fff',
            icon: icon,
            hideAfter: 3500
        });
	}
};

var handleTooltip = {
	_tooltip : function(){
		$('.dotip').tooltip({
            content: function () {
                return $(this).prop('title');
            }
        });
	}
};
var handleClock = {
	digital : function(){
	    var interval = setInterval(function() {
	        var momentNow = moment();
	        $('#date-part').html(momentNow.format('YYYY MMMM DD') + ' '
	                            + momentNow.format('dddd')
	                             .substring(0,3).toUpperCase());
	        $('#time-part').html(momentNow.format('dddd, DD MMMM YYYY | hh : mm A'));
	    }, 100);
	    
	    $('#stop-interval').on('click', function() {
	        clearInterval(interval);
	    });
	}
};
var handleEditor = {
	editor : function(){
		if($("#description").length > 0){
            tinymce.init({
                selector: "textarea#description",
                theme: "modern",
                height:300,
                setup: function (editor) {
			        editor.on('change', function () {
			            tinymce.triggerSave();
			        });
			    },
                plugins: [
                    "advlist autolink lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  print preview fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
    }
};

var progressbar = {
	iUploadHandle : function(b) {
		b = b || false;
		if (b) {
			$('.progress').show();
		} else {
			$('.progress').hide();
		}
		$('.progress-bar').css('width','0%').text('0%');
	}
};