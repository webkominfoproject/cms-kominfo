@extends('layouts.master-without-nav')

@section('content')
        <!-- Begin page -->
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0">
                        <a href="index" class="logo logo-admin"><img src="{{URL::asset('image/gpr-logo.jpg')}}" height="60" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <p class="text-muted text-center">IndonesiaBaik Admin Panel.</p>
                        
                        <form class="form-horizontal m-t-30" action="{{ route('login') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="username">Email</label>
                                <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" id="email" placeholder="Enter email" name="email" value="{{ old('email') }}" autofocus required>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div> 
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Password</label>
                                <input type="password" class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" id="userpassword" placeholder="Enter password" name="password" value="{{ old('password') }}" required>
                            </div>

                            <div class="form-group row m-t-30">
                                <div class="col-12 text-center">
                                    <button class="btn btn-info btn-dark-blue w-md waves-effect waves-light" type="submit">Masuk</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p style="color: white">© {{date('Y')}} IndonesiaBaik.id Admin Panel. by Rumah Konten Indonesia Baik.</p>
            </div>

        </div>
        
@endsection