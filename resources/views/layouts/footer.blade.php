        <footer class="footer">
        © {{date('Y')}} {{ env('APP_NAME') }} <span class="d-none d-sm-inline-block"> - by Rumah Konten Indonesia Baik</span>.
        </footer>