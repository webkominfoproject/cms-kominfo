		<meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="{{ URL::asset('image/gpr-fav.png')}}">

        @yield('css')

        <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <!-- DataTables -->
        <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
         <!-- Bootstrap toogle -->
        <link href="{{ URL::asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('assets/plugins/morris/morris.css')}}">
        <style>
            .side-menu{
                background-color: white;
                box-shadow: 2px 0px 3px -2px rgba(0, 0, 0, 0.15);
            }
            .topbar .topbar-left{
                background: white;
                box-shadow: 2px 0px 3px -2px rgba(0, 0, 0, 0.15);
            }
            #sidebar-menu > ul > li > a.active{
                background: #10a9d7;
            }
            #sidebar-menu > ul > li > a:hover, #sidebar-menu > ul > li > a:focus, #sidebar-menu > ul > li > a:active{
                color: #10a9d7;
            }
            .submenu li a:hover{
                background-color: #10a9d7;
            }
            .submenu li.active > a{
                color: #10a9d7;
                background-color: white;
            }
            .preloader{
    		    width: 100%;
			    height: 100%;
			    top: 0px;
			    position: fixed;
			    z-index: 99999;
			    background: #fdfdfdd6;
			    margin: 0 auto;
			    display: none;
        	}
        	.circular{
                transform-origin: center center;
                width: 250px;
                position: absolute;
                top: 320px;
                bottom: 0;
                left: 0;
                right: 0;
                text-align: center;
                margin: auto;
        	}
            a:hover{
                color: #11a9d7;
            }

            .preloader-ajax-call {
                background: #e9e9e9;  <- I left your 'gray' background
                display: none;        <- Not displayed by default
                position: absolute;   <- This and the following properties will
                top: 0;                  make the overlay, the element will expand
                right: 0;                so as to cover the whole body of the page
                bottom: 0;
                left: 0;
                opacity: 0.5;
            }
        </style>
