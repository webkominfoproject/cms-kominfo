        <!-- jQuery  -->
        <script src="{{ URL::asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/metisMenu.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{ URL::asset('assets/js/waves.min.js')}}"></script>

        <script src="{{ URL::asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
        <!-- Bootstrap toogle -->
        <script src="{{ URL::asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>
        <!-- Morris chart -->
        <script src="{{ URL::asset('assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>
        <!-- Required datatable js -->
        <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/moment/moment.js')}}"></script>

        <!-- Date.Js -->
        <script src="{{ URL::asset('assets/plugins/date/date.js')}}"></script>

        <!-- Datatable init js -->
        <script src="{{ URL::asset('assets/pages/datatables.init.js')}}"></script>
        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.js')}}"></script>
        <script type="text/javascript" src="{{ asset('plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
        <!-- <script type="text/javascript" src="http://malsup.github.com/jquery.form.js"></script> -->
        <script type="text/javascript" src="{{ asset('assets/plugins/malsup/jquery.form.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/validator.js') }}"></script>
        <script type="text/javascript" src="{{ asset("js/init.js")."?v=".date('YmdHis') }}"></script>
        <script type="text/javascript">
                var baseURL = "{{url('')}}";
                var mediaURL = "{{ env('MEDIA_URL') }}";
                @role('superadmin')
                        var levelUser = 1;
                @else
                        var levelUser = 2;
                @endrole
        </script>
        <script type="text/javascript">
                $('#loader-ajax-call').css('display','none');
                if (window.location.href.indexOf("/dashboard") == -1) {
                        $(document).on({
                                ajaxStart: function() { 
                                        $('body').css({opacity: 0.9});
                                        $('#loader-ajax-call').css('display','block');
                                },
                                ajaxStop: function() { 
                                        $('body').css({opacity: 1});
                                        $('#loader-ajax-call').css('display','none');
                                }    
                        });
                }

                $(document).ajaxSuccess(function() {
                        app.init();
                })
                $(".numeric").keydown(function (e) {
                        // Allow: backspace, delete, tab, escape, enter and .
                        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                            // let it happen, don't do anything
                        return;
                }
                        // Ensure that it is a number and stop the keypress
                        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                            e.preventDefault();
                        }
                });
        </script>
        @yield('script-bottom')