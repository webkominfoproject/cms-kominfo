<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131904001-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-131904001-1');
        </script>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>{{ env('APP_NAME') }}</title>
        @include('layouts.head')
  </head>
<body>

        <div class="preloader-ajax-call" id="loader-ajax-call">
            <div class="circular">
                <img class="text-center" src="{{ asset('image/loader.gif') }}" style="margin-bottom:-30px">
                <h5>Harap menunggu ..</h5>
            </div>
        </div>
        <div class="preloader" id="loader">
            <div class="circular">
                <img class="text-center" src="{{ asset('image/loader.gif') }}" style="margin-bottom:-30px">
                <h5>Uploading, Please wait...</h5>
                <div class="progress" style="height: 24px">
                    <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
            </div>
        </div>
        <div class="preloader" id="loaders">
            <div class="circular">
                <img class="text-center" src="{{ asset('image/loader.gif') }}" style="margin-bottom:-30px">
            <h5>Uploading, Please wait...</h5>
            </div>
        </div>
          <!-- Begin page -->
          <div id="wrapper">
      @include('layouts.topbar')
      @include('layouts.sidebar')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
      @yield('content')
                </div> <!-- content -->
    @include('layouts.footer')    
    @include('layouts.modal.master') 
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    @include('layouts.footer-script')    
    </body>
</html>