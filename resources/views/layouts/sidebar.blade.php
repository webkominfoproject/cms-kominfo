            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Menu Utama</li>
                            <li>
                                <a href="{{ route('dashboard.index') }}" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span>
                                </a>
                            </li>
                            @role('superadmin')
                            <li class="menu-title">Manajemen Laporan</li>
                            <li>
                                <a href="{{ route('report.index') }}" class="waves-effect">
                                    <i class="mdi mdi-account-multiple"></i> <span> Laporan </span> 
                                </a>
                            </li>
                            <li class="menu-title">Manajemen pengguna</li>
                            <li>
                                <a href="{{ route('user.index') }}" class="waves-effect">
                                    <i class="mdi mdi-account-multiple"></i> <span> Pengguna Admin </span> 
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('usermember.index') }}" class="waves-effect">
                                    <i class="mdi mdi-account-multiple"></i> <span> Pengguna Member </span> 
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('instansi.index') }}" class="waves-effect">
                                    <i class="mdi mdi-city"></i> <span> Instansi  </span> 
                                </a>
                            </li>
                            <li class="menu-title">Manajemen Konten</li>
                            <li>
                                <a href="{{ route('statuscontent.index') }}" class="waves-effect">
                                    <i class="mdi mdi-tag"></i> <span> Manajemen Konten </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('tags.index') }}" class="waves-effect">
                                    <i class="mdi mdi-tag"></i> <span> Tag </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('topics.index') }}" class="waves-effect">
                                    <i class="mdi mdi-bookmark"></i> <span> Topik </span>
                                </a>
                            </li>
                            @endrole
                            @role('superadmin')
                            <li class="menu-title">Manajemen website</li>
                            <li>
                                <a href="{{ route('fokus.index') }}" class="waves-effect">
                                    <i class="mdi mdi-comment-text-outline"></i> <span> Fokus </span>
                                </a>
                            </li>
                            <!--
                            <li>
                                <a href="{{ route('news.index') }}" class="waves-effect">
                                    <i class="mdi mdi-comment-text-outline"></i> <span> Berita </span>
                                </a>
                            </li>
                            -->
                            <li>
                                <a href="{{ route('pages.index') }}" class="waves-effect">
                                    <i class="mdi mdi-content-copy"></i> <span> Halaman </span>
                                </a>
                            </li>
                            @endrole
                            <li class="menu-title">Manajemen Video</li>
                            <li>
                                <a href="{{route('video.index')}}" class="waves-effect">
                                    <i class="mdi mdi-camcorder"></i> <span> Video </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('category.index') }}" class="waves-effect">
                                    <i class="mdi mdi-bookmark"></i> <span> Kategori Video</span>
                                </a>
                            </li>
                            <li class="menu-title">Manajemen Foto</li>
                            <li>
                            <a href="{{ route('photo.index') }}" class="waves-effect">
                                    <i class="mdi mdi-camera"></i> <span> Foto </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('categoryfoto.index') }}" class="waves-effect">
                                    <i class="mdi mdi-bookmark"></i> <span> Kategori Foto</span>
                                </a>
                            </li>
                            <li class="menu-title">Manajemen Artikel</li>
                            <li>
                            <a href="{{ route('artikel.index') }}" class="waves-effect">
                                    <i class="mdi mdi-file-document-box"></i> <span> Artikel </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('categoryartikel.index') }}" class="waves-effect">
                                    <i class="mdi mdi-bookmark"></i> <span> Kategori Artikel</span>
                                </a>
                            </li>

                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->