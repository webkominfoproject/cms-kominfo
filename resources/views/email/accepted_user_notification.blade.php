@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Selamat !',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <p>Selamat {{$first_name}}, akun admin.gprtv.id anda telah aktif. </p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
        	'title' => 'Login',
        	'link' => 'http://admin.gprtv.id'
    ])
@stop