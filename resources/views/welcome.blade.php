@extends('layouts.master-without-nav')

@section('content')
        <!-- Begin page -->
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0">
                        <a href="index" class="logo logo-admin"><img src="{{URL::asset('image/logo-app.png')}}" height="60" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <p class="text-muted text-center">Accelary Admin Panel.</p>

                        <form class="form-horizontal m-t-30" action="index">

                            <div class="form-group">
                                <label for="username">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Password</label>
                                <input type="password" class="form-control" id="userpassword" placeholder="Enter password" name="password">
                            </div>

                            <div class="form-group row m-t-30">
                                <div class="col-12 text-center">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p>© {{date('Y')}} Accelary Admin Panel. Crafted with <i class="mdi mdi-heart text-danger"></i> by Ibamaulana</p>
            </div>

        </div>
        
@endsection